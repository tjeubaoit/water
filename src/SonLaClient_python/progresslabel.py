__author__ = 'Adm'

from PyQt5 import QtCore, QtWidgets


class ProgressLabel(QtWidgets.QLabel):
    def __init__(self, text, parent=None):
        super(ProgressLabel, self).__init__(parent)

        self.number_dot = 1
        self.main_text = text

        self.progress_timer = QtCore.QTimer()
        self.progress_timer.timeout.connect(self.progress_timer_timeout)
        self.progress_timer.start(500)

        self.timer_to_hide = QtCore.QTimer()
        self.timer_to_hide.setSingleShot(True)
        self.timer_to_hide.timeout.connect(self.hide)

    def __del__(self):
        if self.progress_timer.isActive():
            self.progress_timer.stop()

        if self.timer_to_hide.isActive():
            self.timer_to_hide.stop()

    @QtCore.pyqtSlot()
    def progress_timer_timeout(self):
        if self.number_dot == 3:
            self.number_dot = 1
        else:
            self.number_dot += 1

        self.setText(self.main_text + ' ' + '.' * self.number_dot
                     + (3 - self.number_dot) * ' ' + '\t')

    def hide_delay(self, delay_seconds):
        self.timer_to_hide.start(delay_seconds * 1000)