# -*- coding: utf-8 -*-
__author__ = 'Adm'

URL_PUMP_GIF = "images/pump.gif"
PUMP_GIF_STANDARD_WIDTH = 254
PUMP_GIF_STANDARD_HEIGHT = 192

SPLIT_CHAR = '\n'

STR_REMOTE = 'Từ xa'
STR_LOCAL = 'Tại tủ'

STR_PUMP_ON = 'Tắt bơm'
STR_PUMP_OFF = "Bật bơm"

STYLE_SHEET_BUTTON_ON = """
    QToolButton {
        border:2px solid #191919;
        background-color:#00aa00;
	}
"""
STYLE_SHEET_BUTTON_OFF = """
    QToolButton {
        border:2px solid #191919;
        background-color:#f80000;
    }
"""
STYLE_SHEET_BUTTON_NORMAL = """
    QToolButton {
        border:2px solid #191919;
        background-color:#dedede;
    }
"""

STYLE_SHEET_BUTTON_AUTO = '''
    QPushButton {
        border-image:url(images/auto.png);
    }
'''

STYLE_SHEET_BUTTON_MANUAL = '''
    QPushButton {
        border-image:url(images/manual.png);
    }
'''