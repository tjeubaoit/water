# -*- coding: utf-8 -*-
__author__ = 'Adm'

import sys
import datetime

from PyQt5 import QtCore, QtWidgets, QtGui
import re

import mainwindow1
import cmd_bridge
import defs
import mainwindow2
from reportdialog import ReportDialog
import dbmodel
import cmd_builder
from utils import SendDelayHelper
import settingbox


SEND_FREQUENCY_DELAY = 5000


class MainController(QtCore.QObject):

    __sendCommandToServer__ = QtCore.pyqtSignal([basestring, int], [basestring])

    def __init__(self, parent=None):
        super(MainController, self).__init__(parent)

        self.ui1 = mainwindow1.MainWindow1()
        self.ui1.show()
        self.ui1.setWindowIcon(QtGui.QIcon('images/small.ico'))
        self.ui2 = mainwindow2.MainWindow2()
        self.ui2.show()
        self.ui2.setWindowIcon(QtGui.QIcon('images/small.ico'))

        self.setting_box = None

        self.send_delay_fr1 = SendDelayHelper()
        self.send_delay_fr2 = SendDelayHelper()

        self.last_time_send = {}
        for device_id in defs.device_id_list:
            self.last_time_send[device_id] = datetime.datetime.now()

        self.check_timeout_timer = QtCore.QTimer()
        self.check_timeout_timer.setInterval(1800000)  # 30 minutes
        self.check_timeout_timer.timeout.connect(self.on_check_device_timer_timeout)
        self.check_timeout_timer.start()

        self.check_runmode_timer = QtCore.QTimer()
        self.check_runmode_timer.setInterval(300000) # 5 minutes
        self.check_runmode_timer.timeout.connect(self.on_timerRunMode_timeout)

        self.cmdBridge = cmd_bridge.CommandBridge()

        self._load_configuration_values()
        self._load_device_informations()
        self._register_connections()

        self.cmdBridge.start()

    def __del__(self):
        if self.cmdBridge.isRunning():
            self.cmdBridge.quit()
            self.cmdBridge.wait()
        dbmodel.save_configurations(self.config_dict)

    @QtCore.pyqtSlot(basestring)
    def on_cmdBridge_new_data_update_ui(self, data):
        """
        :type data: str
        :param data:
        :return:
        """
        try:
            params = data.split(',')
            print 'emit', params
            device_id = int(params[0])
            self.last_time_send[device_id] = datetime.datetime.now()

            self._update_ui(device_id, params)
            self._update_status_bar_message('Connected with server')
        except ValueError as ex:
            print 'Error parse record from server', ex.message
            self.ui2.statusbar.showMessage('Error parse record from server' + ex.messag)

    @QtCore.pyqtSlot(basestring)
    def on_cmdBridge_sim_request_result(self, data):
        if self.ui1.isVisible():
            window_showing = self.ui1
        else:
            window_showing = self.ui2
        QtWidgets.QMessageBox.information(window_showing, 'Kết quả lấy sim',
                                          'Số sim thiết bị: ' + data, QtWidgets.QMessageBox.Ok)
        self._update_status_bar_get_sim(False)

    @QtCore.pyqtSlot(int)
    def on_cmdBridge_change_run_mode_result(self, new_mode):
        if new_mode != self.config_dict['bl_run_mode']:
            self.config_dict['bl_run_mode'] = new_mode
            self.ui2.update_run_mode_banlay(new_mode)

        if new_mode == defs.RUN_MODE_AUTO_START:
            self.check_runmode_timer.start()
        else:
            self.check_runmode_timer.stop()

    @QtCore.pyqtSlot(basestring)
    def on_cmdBridge_status_changed(self, text):
        self._update_status_bar_message(text)

    @QtCore.pyqtSlot()
    def on_menu_action_triggered(self):
        action = self.sender()
        widget = action.parentWidget()

        if action == self.ui1.actionQuit or action == self.ui2.actionQuit:
            sys.exit(0)
        elif action == self.ui1.actionSetServerAddr or action == self.ui2.actionSetServerAddr:
            ip_addr, ok = QtWidgets.QInputDialog.getText(widget, "Cấu hình địa chỉ máy chủ",
                                                     "Nhập địa chỉ máy chủ:",
                                                     QtWidgets.QLineEdit.Normal,
                                                     self.config_dict['server_addr'])
            if ok and len(ip_addr) > 0:
                patern = "^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}" \
                         "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
                if re.match(patern, ip_addr):
                    self.config_dict['server_addr'] = ip_addr
                    text = "Cần khởi động lại phần mềm để thay đổi có hiệu lực"
                    ret = QtWidgets.QMessageBox.information(widget, "Thành công",
                                                            text, QtWidgets.QMessageBox.Ok,
                                                            QtWidgets.QMessageBox.Cancel)
                    if ret == QtWidgets.QMessageBox.Ok:
                        sys.exit(0)
                else:
                    text = "Địa chỉ vừa nhập không hợp lệ"
                    QtWidgets.QMessageBox.warning(widget, "Lỗi",
                                                  text, QtWidgets.QMessageBox.Ok)

        elif action == self.ui1.actionReport or action == self.ui2.actionReport:
            reportDlg = ReportDialog.get_instance(widget)
            if reportDlg.isVisible():
                reportDlg.activateWindow()
            else:
                rect = QtWidgets.QDesktopWidget().screenGeometry(widget)
                reportDlg.move(rect.width() / 2 - reportDlg.width() / 2,
                               rect.height() / 2 - reportDlg.height() / 2)
                reportDlg.show()
        elif action == self.ui1.actionShow:
            self.ui2.show()
            self.ui2.activateWindow()
        elif action == self.ui2.actionShow:
            self.ui1.show()
            self.ui1.activateWindow()

    @QtCore.pyqtSlot()
    def on_menu_sim_action_triggered(self):
        action = self.sender()
        if action == self.ui1.actionSim_km7:
            device_id = defs.ID_KM7
        elif action == self.ui1.actionSim_tb:
            device_id = defs.ID_TAYBAC
        elif action == self.ui1.actionSim_p1:
            device_id = defs.ID_PIDLOG1
        elif action == self.ui2.actionSim_bl:
            device_id = defs.ID_BANLAY
        else:
            device_id = defs.ID_PIDLOG2

        self.__sendCommandToServer__[basestring].emit(cmd_builder.create_command_get_sim(device_id))
        self._update_status_bar_get_sim(True)

    @QtCore.pyqtSlot(bool)
    def on_btnControls_clicked(self):
        button = self.sender()
        if button == self.ui1.btnOffPump1_km7:
            cmd = cmd_builder.create_command_switch_pump(defs.ID_KM7, 1, False)
        elif button == self.ui1.btnOnPump1_km7:
            cmd = cmd_builder.create_command_switch_pump(defs.ID_KM7, 1, True)
        elif button == self.ui1.btnOffPump2_km7:
            cmd = cmd_builder.create_command_switch_pump(defs.ID_KM7, 2, False)
        elif button == self.ui1.btnOnPump2_km7:
            cmd = cmd_builder.create_command_switch_pump(defs.ID_KM7, 2, True)

        elif button == self.ui1.btnOnV1_tb:
            cmd = cmd_builder.create_command_switch_pump(defs.ID_TAYBAC, 3, True)
        elif button == self.ui1.btnOffV1_tb:
            cmd = cmd_builder.create_command_switch_pump(defs.ID_TAYBAC, 3, False)

        elif button == self.ui2.btnOnPump1_bl:
            cmd = cmd_builder.create_command_switch_pump(defs.ID_BANLAY, 1, True)
        elif button == self.ui2.btnOffPump1_bl:
            cmd = cmd_builder.create_command_switch_pump(defs.ID_BANLAY, 1, False)
        elif button == self.ui2.btnOnPump2_bl:
            cmd = cmd_builder.create_command_switch_pump(defs.ID_BANLAY, 2, True)
        elif button == self.ui2.btnOffPump2_bl:
            cmd = cmd_builder.create_command_switch_pump(defs.ID_BANLAY, 2, False)

        elif button == self.ui2.btnOnVan1_bl:
            cmd = cmd_builder.create_command_switch_pump(defs.ID_BANLAY, 3, True)
        elif button == self.ui2.btnOffVan1_bl:
            cmd = cmd_builder.create_command_switch_pump(defs.ID_BANLAY, 3, False)

        self.__sendCommandToServer__.emit(cmd, 0)

    @QtCore.pyqtSlot(int)
    def on_spinboxFrequency_value_changed(self, val):
        spin = self.sender()
        if spin == self.ui2.spFrequency1_bl:
            cmd = cmd_builder.create_command_set_frequency(defs.ID_BANLAY, 1, val)
            self.send_delay_fr1.send(data=cmd, delay_miliseconds=SEND_FREQUENCY_DELAY)
            self.config_dict['bl_f1'] = val

        elif spin == self.ui2.spFrequency2_bl:
            cmd = cmd_builder.create_command_set_frequency(defs.ID_BANLAY, 2, val)
            self.send_delay_fr2.send(data=cmd, delay_miliseconds=SEND_FREQUENCY_DELAY)
            self.config_dict['bl_f2'] = val

    @QtCore.pyqtSlot()
    def on_check_device_timer_timeout(self):
        now = datetime.datetime.now()
        for key in self.last_time_send.keys():
            if now >= self.last_time_send[key] + datetime.timedelta(minutes=30):
                self._update_ui(key, None)

    @QtCore.pyqtSlot()
    def on_btnRunMode_clicked(self):
        btn = self.sender()
        if btn == self.ui2.btnRunMode_bl:
            if self.config_dict['bl_run_mode'] == defs.RUN_MODE_MANUAL:
                msg = 'Chuyển sang chế độ chạy tự động ?'
            else:
                msg = 'Chuyển sang chế độ chạy tay ?'

            ret = QtWidgets.QMessageBox.question(self.ui2, 'Đổi chế độ chạy bơm', msg,
                                                 QtWidgets.QMessageBox.Ok,
                                                 QtWidgets.QMessageBox.Cancel)
            if ret == QtWidgets.QMessageBox.Cancel:
                return
            cmd = cmd_builder.create_command_change_run_mode(defs.ID_BANLAY,
                                                             not self.config_dict['bl_run_mode'])
        elif btn == self.ui2.btnAutoRunStart_bl:
            cmd = cmd_builder.create_command_change_run_mode(defs.ID_BANLAY,
                                                             defs.RUN_MODE_AUTO_START)
        elif btn == self.ui2.btnAutoRunStop_bl:
            cmd = cmd_builder.create_command_change_run_mode(defs.ID_BANLAY,
                                                             defs.RUN_MODE_AUTO_STOP)
        else:
            return

        self.__sendCommandToServer__.emit(cmd, 3)

    @QtCore.pyqtSlot()
    def on_timerRunMode_timeout(self):
        self.__sendCommandToServer__[basestring].emit(
            cmd_builder.create_command_get_run_mode(defs.ID_BANLAY))

    @QtCore.pyqtSlot()
    def on_btnSettingAutoRun_clicked(self):
        pressure = self.ui2.spPress.value()
        time_limit = int(self.ui2.editTimeLimit.text())
        time_unit = self.ui2.cbTimeUnit.currentIndex()
        time_unit_data = self.ui2.cbTimeUnit.currentData()
        cmd = cmd_builder.create_command_config_auto_run(defs.ID_BANLAY, pressure,
                                                         time_limit * time_unit_data)
        self.__sendCommandToServer__.emit(cmd, 5)

        self.config_dict['st_press'] = pressure
        self.config_dict['st_time_limit'] = time_limit
        self.config_dict['st_time_unit'] = time_unit

    @QtCore.pyqtSlot()
    def on_app_about_to_quit(self):
        if self.config_dict['bl_run_mode'] == defs.RUN_MODE_AUTO_START:
            self.config_dict['bl_run_mode'] = defs.RUN_MODE_AUTO_STOP
            self.ui2.btnAutoRunStop_bl.click()
        dbmodel.save_configurations(self.config_dict)

    def _register_connections(self):
        QtCore.QCoreApplication.instance().aboutToQuit.connect(self.on_app_about_to_quit)

        # window 1
        self.ui1.btnOnPump1_km7.clicked.connect(self.on_btnControls_clicked)
        self.ui1.btnOffPump1_km7.clicked.connect(self.on_btnControls_clicked)
        self.ui1.btnOnPump2_km7.clicked.connect(self.on_btnControls_clicked)
        self.ui1.btnOffPump2_km7.clicked.connect(self.on_btnControls_clicked)

        self.ui1.btnOffV1_tb.clicked.connect(self.on_btnControls_clicked)
        self.ui1.btnOnV1_tb.clicked.connect(self.on_btnControls_clicked)

        self.ui1.actionShow.triggered.connect(self.on_menu_action_triggered)
        self.ui1.actionQuit.triggered.connect(self.on_menu_action_triggered)
        self.ui1.actionReport.triggered.connect(self.on_menu_action_triggered)
        self.ui1.actionSetServerAddr.triggered.connect(self.on_menu_action_triggered)

        for action in self.ui1.menuSim.actions():
            action.triggered.connect(self.on_menu_sim_action_triggered)

        # window 2

        self.ui2.btnOnVan1_bl.clicked.connect(self.on_btnControls_clicked)
        self.ui2.btnOffVan1_bl.clicked.connect(self.on_btnControls_clicked)

        self.ui2.btnOnPump1_bl.clicked.connect(self.on_btnControls_clicked)
        self.ui2.btnOffPump1_bl.clicked.connect(self.on_btnControls_clicked)
        self.ui2.btnOnPump2_bl.clicked.connect(self.on_btnControls_clicked)
        self.ui2.btnOffPump2_bl.clicked.connect(self.on_btnControls_clicked)

        self.ui2.actionShow.triggered.connect(self.on_menu_action_triggered)
        self.ui2.actionQuit.triggered.connect(self.on_menu_action_triggered)
        self.ui2.actionReport.triggered.connect(self.on_menu_action_triggered)
        self.ui2.actionSetServerAddr.triggered.connect(self.on_menu_action_triggered)

        self.ui2.spFrequency1_bl.valueChanged.connect(self.on_spinboxFrequency_value_changed)
        self.ui2.spFrequency2_bl.valueChanged.connect(self.on_spinboxFrequency_value_changed)

        self.ui2.btnRunMode_bl.clicked.connect(self.on_btnRunMode_clicked)
        self.ui2.btnAutoRunStart_bl.clicked.connect(self.on_btnRunMode_clicked)
        self.ui2.btnAutoRunStop_bl.clicked.connect(self.on_btnRunMode_clicked)
        self.ui2.btnSettingAutoRun.clicked.connect(self.on_btnSettingAutoRun_clicked)

        for action in self.ui2.menuSim.actions():
            action.triggered.connect(self.on_menu_sim_action_triggered)

        # logic connections
        self.send_delay_fr1.__sendDelayTimeout__.connect(self.__sendCommandToServer__[basestring])
        self.send_delay_fr2.__sendDelayTimeout__.connect(self.__sendCommandToServer__[basestring])

        self.__sendCommandToServer__.connect(self.cmdBridge.handle_command_from_controller)
        self.__sendCommandToServer__[basestring].connect(self.cmdBridge.handle_command_from_controller)

        self.cmdBridge.__newRecordUpdateUi__.connect(self.on_cmdBridge_new_data_update_ui)
        self.cmdBridge.__requestSimSuccess__.connect(self.on_cmdBridge_sim_request_result)
        self.cmdBridge.__changeRunModeSuccess__.connect(self.on_cmdBridge_change_run_mode_result)
        self.cmdBridge.__statusChanged__.connect(self.on_cmdBridge_status_changed)

    def _load_configuration_values(self):
        self.config_dict = dbmodel.load_configurations()

        if self.config_dict:
            self.ui2.spFrequency1_bl.setValue(self.config_dict['bl_f1'])
            self.ui2.spFrequency2_bl.setValue(self.config_dict['bl_f2'])
            self.ui2.update_run_mode_banlay(self.config_dict['bl_run_mode'])
        else:
            self.config_dict = dict()
            self.config_dict['bl_f1'] = self.ui2.spFrequency1_bl.value()
            self.config_dict['bl_f2'] = self.ui2.spFrequency2_bl.value()
            self.config_dict['bl_run_mode'] = defs.RUN_MODE_MANUAL
            self.config_dict['st_press'] = 0.0
            self.config_dict['st_time_limit'] = 0
            self.config_dict['st_time_unit'] = 1
            self.config_dict['server_addr'] = '127.0.0.1'

        if not self.config_dict['server_addr']:
            self.config_dict['server_addr'] = '127.0.0.1'
        cmd_bridge.SERVER_ADDR = self.config_dict['server_addr']

        init_values = (self.config_dict['st_press'],
                       self.config_dict['st_time_limit'],
                       self.config_dict['st_time_unit'])
        self.setting_box = settingbox.SettingBox(self.ui2, init_values)

    def _load_device_informations(self):
        device_info = dbmodel.load_device_informations()
        print 'device id list loaded:', device_info
        if device_info:
            defs.update_device_information(device_info)
        else:
            self._update_status_bar_message('Device ID configuration not found')

    def _update_ui(self, device_id, params):
        if device_id == defs.ID_KM7:
            self.ui1.update_ui_km7(params)
        elif device_id == defs.ID_TAYBAC:
            self.ui1.update_ui_taybac(params)
        elif device_id == defs.ID_PIDLOG2:
            self.ui1.update_ui_p2(params)
        elif device_id == defs.ID_PIDLOG1:
            self.ui2.update_ui_p1(params)
        elif device_id == defs.ID_BANLAY:
            self.ui2.update_ui_banlay(params)

    def _update_status_bar_message(self, text):
        self.ui1.statusbar.showMessage(text)
        self.ui2.statusbar.showMessage(text)

    def _update_status_bar_get_sim(self, visible):
        self.ui1._update_status_get_sim(visible)
        self.ui2._update_status_get_sim(visible)
