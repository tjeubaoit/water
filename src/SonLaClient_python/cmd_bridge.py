__author__ = 'Adm'

import time

from PyQt5 import QtCore, QtNetwork

import defs
import res


SERVER_PORT = 6969
SERVER_ADDR = '127.0.0.1'


class CommandBridge(QtCore.QThread):
    """
    Receive data from server and push to update UI
    Receiver control from use and push command to server
    """
    __newRecordUpdateUi__ = QtCore.pyqtSignal(basestring)
    __requestSimSuccess__ = QtCore.pyqtSignal(basestring)
    __changeRunModeSuccess__ = QtCore.pyqtSignal(int)
    __statusChanged__ = QtCore.pyqtSignal(basestring)

    def __init__(self, parent=None):
        super(CommandBridge, self).__init__(parent)
        self.moveToThread(self)

        self.socket = QtNetwork.QTcpSocket()
        self.socket.connected.connect(self.socket_connected)
        self.socket.disconnected.connect(self.socket_disconnected)
        self.socket.error.connect(self.socket_error)
        self.socket.readyRead.connect(self.handle_data_from_server)
        self.socket.moveToThread(self)

        self.buffer = bytearray()

        self.frequency_text = ['', '']
        self.frequency_wait_for_send = [0, 0]

    def __del__(self):
        if self.socket.isOpen():
            self.socket.disconnectFromHost()
            self.socket.close()

    def run(self):
        self.socket.connectToHost(QtNetwork.QHostAddress(SERVER_ADDR), SERVER_PORT)
        self.exec_()

    @QtCore.pyqtSlot(basestring, int)
    @QtCore.pyqtSlot(basestring)
    def handle_command_from_controller(self, cmd, repeat=0):
        if repeat == 0:
            self.socket.write(cmd)
            print 'send ->', cmd
        else:
            count = 0
            while count < repeat:
                print 'send ->', cmd
                self.socket.write(cmd)
                self.socket.flush()
                time.sleep(0.5)
                count += 1

    @QtCore.pyqtSlot()
    def handle_data_from_server(self):
        while not self.socket.atEnd():
            self.buffer.extend(self.socket.readAll())

        while res.SPLIT_CHAR in self.buffer:
            index = self.buffer.index(res.SPLIT_CHAR)
            emit_data = str(self.buffer[:index])

            try:
                if int(emit_data[:3]) in defs.device_id_list:
                    self.__newRecordUpdateUi__.emit(emit_data)

                    if emit_data.startswith(str(defs.ID_PIDLOG1)):
                        list_pidlod1 = emit_data.split(',')
                        if len(list_pidlod1) >= 1:
                            text = str(defs.ID_BANLAY) + ',PR=' + list_pidlod1[1] + '#'
                            self.socket.write(text)
            except ValueError as ex:
                sim_dectection_text = 'Ban dang o MHAN04H. So may cua ban la:'
                run_mode_dectection_text = 'AUTO'

                if sim_dectection_text[:8] in emit_data:
                    try:
                        index = emit_data.index(sim_dectection_text[:8])
                        end = emit_data.rindex('"')
                        sim_text = emit_data[index + len(sim_dectection_text) + 1:end]
                        self.__requestSimSuccess__.emit(sim_text)
                        print 'Phone number found:', sim_text
                    except ValueError as e:
                        print e
                        self.__statusChanged__.emit('Exception: ' + e.message)

                elif run_mode_dectection_text in emit_data:
                    if 'ON' in emit_data:
                        self.__changeRunModeSuccess__.emit(defs.RUN_MODE_AUTO)
                    elif 'OFF' in emit_data:
                        self.__changeRunModeSuccess__.emit(defs.RUN_MODE_MANUAL)
                    elif 'START' in emit_data:
                        self.__changeRunModeSuccess__.emit(defs.RUN_MODE_AUTO_START)
                    elif 'STOP' in emit_data or 'FINISH' in emit_data:
                        self.__changeRunModeSuccess__.emit(defs.RUN_MODE_AUTO_STOP)

            del self.buffer[:index + 1]

    @QtCore.pyqtSlot()
    def socket_connected(self):
        print 'socket connected'
        self.socket.write('vtekadmin')
        self.__statusChanged__.emit('Connected with server')

    @QtCore.pyqtSlot()
    def socket_disconnected(self):
        print 'socket disconnected'
        self.__statusChanged__.emit('Disconnected with server')
        self.socket.connectToHost(QtNetwork.QHostAddress(SERVER_ADDR), SERVER_PORT)

    @QtCore.pyqtSlot(QtNetwork.QAbstractSocket.SocketError)
    def socket_error(self, error):
        print 'socket error'
        self.__statusChanged__.emit('Connect to server error')
        self.socket.connectToHost(QtNetwork.QHostAddress(SERVER_ADDR), SERVER_PORT)
