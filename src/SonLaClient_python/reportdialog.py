# -*- coding: utf-8 -*-
__author__ = 'Adm'

from datetime import datetime

from PyQt5 import QtCore, QtWidgets

import utils
import dbmodel
import defs
import excel
from ui import ui_reportdialog


class ReportDialog(QtWidgets.QDialog):
    _instance = None

    @classmethod
    def get_instance(cls, parent=None):
        if not ReportDialog._instance:
            ReportDialog._instance = cls(parent)

        return ReportDialog._instance


    def __init__(self, parent=None):
        super(ReportDialog, self).__init__(parent)
        self.setWindowFlags(self.windowFlags()
                            | QtCore.Qt.WindowSystemMenuHint
                            | QtCore.Qt.WindowMinMaxButtonsHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)

        self.ui = ui_reportdialog.Ui_Dialog()
        self.ui.setupUi(self)

        self.statusbar = QtWidgets.QStatusBar()
        self.statusbar.showMessage('Database path: ' + dbmodel.get_default_path().replace('/', '\\'))
        self.setStyleSheet('''QStatusBar { border-top:1px solid #aaaaaa; } ''')
        self.layout().addWidget(self.statusbar)

        self.ui.cbDevice.addItem('RTU Km7', defs.ID_KM7)
        self.ui.cbDevice.addItem('RTU Đại học Tây Bắc', defs.ID_TAYBAC)
        self.ui.cbDevice.addItem('RTU Bản Lay', defs.ID_BANLAY)
        self.ui.cbDevice.addItem('Điểm bất lợi 1', defs.ID_PIDLOG1)
        self.ui.cbDevice.addItem('Điểm bất lợi 2', defs.ID_PIDLOG2)

        self.ui.dtLast.setDateTime(datetime.now())
        self.ui.tableWidget.horizontalHeader().setSectionsMovable(True)

        self.ui.btnExportSelectFinish.clicked.connect(self._btnExportSelectFinish_clicked)
        self.ui.btnExcel.clicked.connect(self._btnExcel_clicked)

        self.records = []
        self.headers = []

    def __del__(self):
        pass

    @QtCore.pyqtSlot()
    def _btnExportSelectFinish_clicked(self):
        device_id = int(self.ui.cbDevice.currentData())
        nColumns = 0
        if device_id == defs.ID_KM7:
            nColumns = 12
        elif device_id == defs.ID_TAYBAC:
            nColumns = 8
        elif device_id == defs.ID_BANLAY:
            nColumns = 16
        elif device_id == defs.ID_PIDLOG2 or device_id == defs.ID_PIDLOG1:
            nColumns = 4
        else:
            self.statusbar.showMessage('Device id ' + device_id + ' invalid')
            return

        self.ui.tableWidget.clear()
        self.ui.tableWidget.setColumnCount(nColumns)
        self.ui.tableWidget.setRowCount(0)
        del self.headers[:]

        for i in range(0, nColumns):
            self.headers.append(utils.get_header_table(device_id, i))
        self.ui.tableWidget.setHorizontalHeaderLabels(self.headers)

        first = self.ui.dtFirst.dateTime().toPyDateTime()
        last = self.ui.dtLast.dateTime().toPyDateTime()

        self.records = dbmodel.get_rows_by_time(device_id, first, last)
        if self.records is None:
            self.statusbar.showMessage('No result, has error when query database')
            return

        self.ui.tableWidget.setRowCount(len(self.records))
        row_count = 0
        for row in self.records:
            for tuple_id in range(0, len(row)):
                item = QtWidgets.QTableWidgetItem(str(row[tuple_id]))
                self.ui.tableWidget.setItem(row_count, tuple_id, item)

            row_count = row_count + 1

        self.ui.tableWidget.resizeColumnsToContents()

    @QtCore.pyqtSlot()
    def _btnExcel_clicked(self):
        filePath = QtWidgets.QFileDialog.getSaveFileName(self, 'Save file', '',
                                                         'Excel 97-2003 Workbook (*.xls)')
        if not filePath[0]:
            self.statusbar.showMessage('Path to save xls file is not available')
            return

        excel.export_to_excel(sheet_name=self.ui.cbDevice.currentText(),
                              headers=self.headers,
                              records=self.records,
                              file_path=filePath[0].encode(encoding='utf-8'))