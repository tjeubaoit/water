__author__ = 'Adm'

import sys

from PyQt5 import QtWidgets, QtGui

from maincontroller import MainController


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon('images/small.ico'))
    controller = MainController()
    sys.exit(app.exec_())
