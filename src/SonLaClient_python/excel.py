__author__ = 'Adm'

import xlwt


def export_to_excel(sheet_name, headers, records, file_path):
    workbook = xlwt.Workbook(encoding='utf-8')
    worksheet = workbook.add_sheet(sheetname=sheet_name, cell_overwrite_ok=True)

    for index in range(0, len(headers)):
        worksheet.write(0, index, headers[index])

    row_id = 1
    for row in records:
        for index in range(0, len(row)):
            worksheet.write(row_id, index, row[index])
        row_id += 1

    workbook.save(file_path)
