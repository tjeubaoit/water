# -*- coding: utf-8 -*-
__author__ = 'Adm'

import defs
from PyQt5 import QtCore


def get_header_table(device_id, column_id):
    if device_id == defs.ID_KM7:
        return get_header_table_km7(column_id)
    elif device_id == defs.ID_TAYBAC:
        return get_header_table_tb(column_id)
    elif device_id == defs.ID_BANLAY:
        return get_header_table_bl(column_id)
    elif device_id == defs.ID_PIDLOG1 or device_id == defs.ID_PIDLOG2:
        return get_header_table_pidlogs(column_id)
    else:
        return None


def get_header_table_km7(id):
    if id == defs.ID_KM7_PUMP2:
        return 'Trạng thái bơm 2'
    elif id == defs.ID_KM7_TOTAL_FLOW:
        return 'Thời gian chạy bơm 2'
    elif id == defs.ID_KM7_CONTROL_STATUS:
        return 'Chế độ điều khiển'
    elif id == defs.ID_KM7_FLOW:
        return 'Lưu lượng'
    elif id == defs.ID_KM7_LEVEL:
        return 'Mức nước'
    elif id == defs.ID_KM7_PRESSURE:
        return 'Áp suất'
    elif id == defs.ID_KM7_PUMP1:
        return 'Trạng thái bơm 1'
    elif id == defs.ID_KM7_TIME1:
        return 'Thời gian chạy bơm 1'
    elif id == defs.ID_KM7_TIME2:
        return 'Lưu lượng tổng'
    elif id == defs.ID_KM7_SIGNAL:
        return 'Mức tín hiệu'
    elif id == defs.ID_KM7_VOLT:
        return 'Điện áp'
    elif id == defs.ID_TIMESERVER:
        return 'Thời gian'
    else:
        return None


def get_header_table_tb(id):
    if id == defs.ID_TB_VAN1:
        return 'Trạng thái van 1'
    elif id == defs.ID_TB_VAN2:
        return 'Trạng thái van 2'
    elif id == defs.ID_TB_TIME1:
        return 'Thời gian chạy van 1'
    elif id == defs.ID_TB_TIME2:
        return 'Thời gian chạy van 2'
    elif id == defs.ID_TB_CONTROL_STATUS:
        return 'Chế độ điều khiển'
    elif id == defs.ID_KM7_SIGNAL:
        return 'Mức tín hiệu'
    elif id == defs.ID_KM7_VOLT:
        return 'Điện áp'
    elif id == defs.ID_TIMESERVER:
        return 'Thời gian'
    else:
        return None


def get_header_table_bl(id):
    if id == defs.ID_BL_PUMP2:
        return 'Trạng thái bơm 2'
    elif id == defs.ID_BL_TOTAL_FLOW:
        return 'Thời gian chạy bơm 2'
    elif id == defs.ID_BL_CONTROL_STATUS:
        return 'Chế độ điều khiển'
    elif id == defs.ID_BL_FLOW:
        return 'Lưu lượng'
    elif id == defs.ID_BL_LEVEL:
        return 'Mức nước'
    elif id == defs.ID_BL_PRESSURE:
        return 'Áp suất'
    elif id == defs.ID_BL_PUMP1:
        return 'Trạng thái bơm 1'
    elif id == defs.ID_BL_TIME1:
        return 'Thời gian chạy bơm 1'
    elif id == defs.ID_BL_TIME2:
        return 'Lưu lượng tổng'
    elif id == defs.ID_BL_SIGNAL:
        return 'Mức tín hiệu'
    elif id == defs.ID_BL_VOLT:
        return 'Điện áp'
    elif id == defs.ID_TIMESERVER:
        return 'Thời gian'
    elif id == defs.ID_FREQUENCY_PUMP1:
        return 'Tần số bơm 1'
    elif id == defs.ID_FREQUENCY_PUMP2:
        return 'Tần số bơm 2'
    elif id == defs.ID_BL_VAN1:
        return 'Trạng thái van 1'
    elif id == defs.ID_BL_VAN2:
        return 'Trạng thái van 2'
    else:
        return None


def get_header_table_pidlogs(id):
    if id == defs.ID_PL_PRESSURE:
        return 'Áp suất'
    elif id == defs.ID_PL_VOLT_DB:
        return 'Điện áp'
    elif id == defs.ID_PL_SIGNAL_DB:
        return 'Tín hiệu'
    elif id == defs.ID_TIMESERVER:
        return 'Thời gian'


def parse_param(param, scale=1):
    """
    :type param: str
    :return: str
    """
    if scale > 1:
        return str(float(param) / scale)
    else:
        return str(int(param))


class SendDelayHelper(QtCore.QObject):

    __sendDelayTimeout__ = QtCore.pyqtSignal(basestring)

    def __init__(self, parent=None):
        super(SendDelayHelper, self).__init__(parent)

        self.timer = QtCore.QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.timer_timeout)

        self.data = None

    def __del__(self):
        if self.timer.isActive():
            self.timer.stop()

    def send(self, data, delay_miliseconds):
        self.data = data
        self.timer.start(delay_miliseconds)

    @QtCore.pyqtSlot()
    def timer_timeout(self):
        self.__sendDelayTimeout__.emit(self.data)