__author__ = 'Adm'

import defs


def create_command_switch_pump(device_id, pump_id, pump_status):
    if pump_id < 3:
        text = str(device_id) + ',P' + str(pump_id) + '=' + str(int(pump_status)) + '#'
    else:
        text = str(device_id) + ',V' + str(pump_id - 2) + '=' + str(int(pump_status)) + '#'
    return text


def create_command_set_frequency(device_id, pump_id, frequency):
    frequency_text = str(int(frequency))
    frequency_text = '=' + (2 - len(frequency_text)) * '0' + frequency_text

    return str(device_id) + ',F' + str(pump_id) + frequency_text + '#'


def create_command_config_auto_run(device_id, pressure, second_time_limit):
    if second_time_limit > 999999 or pressure >= 100:
        return None

    pressure_text = str(int(pressure * 100))
    pressure_text = (4 - len(pressure_text)) * '0' + pressure_text
    time_text = str(second_time_limit)
    time_text = (6 - len(time_text)) * '0' + time_text

    return str(device_id) + ',' + 'CF=' + pressure_text + ',' + time_text + '#'


def create_command_get_run_mode(device_id):
    return str(device_id) + ',MODE?#'


def create_command_get_sim(device_id):
    return str(device_id) + ',GETN#'


def create_command_change_run_mode(device_id, mode):
    return str(device_id) + ',AT=' + str(int(mode)) + '#'