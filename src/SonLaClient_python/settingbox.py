# -*- coding: utf-8 -*-
__author__ = 'Adm'

import sys

from PyQt5 import QtCore, QtGui


class SettingBox(QtCore.QObject):
    def __init__(self, ui, loaded_values, parent=None):
        super(SettingBox, self).__init__(parent)

        self.ui = ui

        self.ui.cbTimeUnit.addItem('Giây', 1)
        self.ui.cbTimeUnit.addItem('Phút', 60)
        self.ui.cbTimeUnit.addItem('Giờ', 3600)

        self.ui.spPress.setValue(loaded_values[0])
        self.ui.editTimeLimit.setText(str(loaded_values[1]))
        self.ui.cbTimeUnit.setCurrentIndex(loaded_values[2])
        self.ui.editTimeLimit.setValidator(QtGui.QIntValidator(0, sys.maxint))

    def __del__(self):
        pass