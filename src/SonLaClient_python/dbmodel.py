# -*- coding: utf-8 -*-
__author__ = 'Adm'

import sqlite3
import os
from datetime import datetime
from datetime import timedelta

import defs


def get_default_path():
    if not os.environ.has_key('ALLUSERSPROFILE'):
        return None

    path = os.getenv('ALLUSERSPROFILE')
    if os.path.exists(path + '/SonLaServer'):
        path = path + '/SonLaServer/sonla.db'
    elif os.path.exists(path + '/Application Data/SonLaServer'):
        path = path + '/Application Data/SonLaServer/sonla.db'
    else:
        return None

    return path


def get_default_local_path():
    if not os.environ.has_key('APPDATA'):
        return None

    path = os.getenv('APPDATA') + '/SonLaClient'
    if not os.path.exists(path):
        os.makedirs(path)

    return path + '/sonlaclient.db'


def get_table_name(device_id):
    if device_id == defs.ID_KM7:
        return 'km7'
    elif device_id == defs.ID_BANLAY:
        return 'banlay'
    elif device_id == defs.ID_TAYBAC:
        return 'taybac'
    elif device_id == defs.ID_PIDLOG1:
        return 'pidlog1'
    elif device_id == defs.ID_PIDLOG2:
        return 'pidlog2'
    else:
        return None


def get_field_names(device_id):
    if device_id == defs.ID_KM7:
        field_names = '''timeserver, signal, volt, press, level, flow, pump1, pump2,
                        control_state, time1, time2, total_flow'''
    elif device_id == defs.ID_TAYBAC:
        field_names = 'timeserver, signal, volt, van1, van2, control_state, time1, time2'
    elif device_id == defs.ID_BANLAY:
        field_names = '''timeserver, signal, volt, press, level, flow, pump1, pump2,
                        van1, van2, control_state, time1, time2, total_flow,
                        frequency1, frequency2'''
    elif device_id == defs.ID_PIDLOG1 or device_id == defs.ID_PIDLOG2:
        field_names = 'timeserver, press, signal, volt'
    else:
        return None

    return field_names


def floor_datetime(dt):
    """
    :type dt: datetime
    :return:
    """
    minute = dt.minute
    hour = dt.hour

    addMinutes = 0
    while minute % 5 != 0:
        addMinutes = addMinutes + 1

    return dt + timedelta(minutes=addMinutes)


def isoformat_to_datetime(formatted_str):
    """
    :type formatted_str: str
    :param formatted_str:
    :return:
    """
    try:
        dt_str = formatted_str[:formatted_str.rindex('.')]
    except ValueError:
        dt_str = formatted_str
    return datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S')


def logic_to_text(logic_state, text_when_true='Bật', text_when_false='Tắt'):
    if logic_state:
        return text_when_true
    else:
        return text_when_false


def get_rows_by_time(device_id, first, last, inteval=300, flag=0):
    """
    :type first: datetime
    :type last: datetime
    :return: list
    """
    db_path = get_default_path()
    if not os.path.isfile(db_path):
        print 'db not found'
        return

    field_names = get_field_names(device_id)
    table_name = get_table_name(device_id)

    if not table_name or not field_names:
        return

    query = """SELECT %s from %s where datetime(timeserver, 'localtime') >= datetime(?, 'localtime')
            AND datetime(timeserver, 'localtime') < datetime(?, 'localtime')""" % (field_names, table_name)

    db = sqlite3.connect(database=db_path, cached_statements=1000)
    cursor = db.cursor()
    cursor.execute(query, (first.isoformat(), last.isoformat()))

    result = []
    next = floor_datetime(first)

    while True:
        row = cursor.fetchone()
        if row is None:
            break

        timeserver = isoformat_to_datetime(row[defs.ID_TIMESERVER])
        if timeserver >= next:
            while timeserver > next + timedelta(seconds=inteval):
                next = next + timedelta(seconds=inteval)
            next = next + timedelta(seconds=inteval)

            tmp = list(row)

            dt_str = tmp[defs.ID_TIMESERVER]
            dt = datetime.strptime(dt_str[:dt_str.rfind('.')], '%Y-%m-%d %H:%M:%S')
            tmp[defs.ID_TIMESERVER] = dt.strftime('%d-%m-%Y %H:%M')

            if device_id == defs.ID_KM7:
                tmp[defs.ID_KM7_PUMP1] = logic_to_text(tmp[defs.ID_KM7_PUMP1])
                tmp[defs.ID_KM7_PUMP2] = logic_to_text(tmp[defs.ID_KM7_PUMP2])
                tmp[defs.ID_KM7_CONTROL_STATUS] = logic_to_text(tmp[defs.ID_KM7_CONTROL_STATUS],
                                                                text_when_true='Tại tủ',
                                                                text_when_false='Từ xa')
            elif device_id == defs.ID_TAYBAC:
                tmp[defs.ID_TB_VAN1] = logic_to_text(tmp[defs.ID_TB_VAN1], 'Mở', 'Đóng')
                tmp[defs.ID_TB_VAN2] = logic_to_text(tmp[defs.ID_TB_VAN2], 'Mở', 'Đóng')
                tmp[defs.ID_TB_CONTROL_STATUS] = logic_to_text(tmp[defs.ID_TB_CONTROL_STATUS],
                                                               text_when_false='Từ xa',
                                                               text_when_true='Tại tủ')
            elif device_id == defs.ID_BANLAY:
                tmp[defs.ID_BL_PUMP1] = logic_to_text(tmp[defs.ID_BL_PUMP1])
                tmp[defs.ID_BL_PUMP2] = logic_to_text(tmp[defs.ID_BL_PUMP2])
                tmp[defs.ID_BL_CONTROL_STATUS] = logic_to_text(tmp[defs.ID_BL_CONTROL_STATUS],
                                                               text_when_true='Tại tủ',
                                                               text_when_false='Từ xa')
                tmp[defs.ID_BL_VAN1] = logic_to_text(tmp[defs.ID_BL_VAN1], 'Mở', 'Đóng')
                tmp[defs.ID_BL_VAN2] = logic_to_text(tmp[defs.ID_BL_VAN2], 'Mở', 'Đóng')

            row = tuple(tmp)
            result.append(row)

    return result


def save_configurations(config_dict):
    """
    insert or update configuration values into db
    :param config_dict:
    :return:
    """
    db_path = get_default_local_path()
    if not db_path:
        return

    db = sqlite3.connect(database=db_path, cached_statements=1000)
    cursor = db.cursor()

    cursor.execute('''CREATE TABLE IF NOT EXISTS config (
                    id INTEGER PRIMARY KEY,
                    bl_frequency1 INTEGER,
                    bl_frequency2 INTEGER,
                    bl_run_mode INTEGER,
                    setting_press_p1 INTEGER,
                    setting_time_limit INTEGER,
                    setting_time_unit INTEGER,
                    server_addr TEXT
    );''')

    query = '''INSERT OR REPLACE INTO config (id, bl_frequency1, bl_frequency2, bl_run_mode,
                setting_press_p1, setting_time_limit, setting_time_unit, server_addr)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?)'''
    cursor.execute(query, (1, config_dict['bl_f1'],
                           config_dict['bl_f2'],
                           config_dict['bl_run_mode'],
                           config_dict['st_press'],
                           config_dict['st_time_limit'],
                           config_dict['st_time_unit'],
                           config_dict['server_addr'])
    )

    db.commit()
    db.close()


def load_configurations():
    """
    load configuration values from db
    :return:
    """
    db_path = get_default_local_path()
    if not os.path.isfile(db_path):
        print 'load db failed. db not found'
        return None

    db = sqlite3.connect(database=db_path, cached_statements=1000)
    cursor = db.cursor()

    try:
        query = '''SELECT bl_frequency1, bl_frequency2, bl_run_mode, setting_press_p1,
                setting_time_limit, setting_time_unit, server_addr FROM config'''
        cursor.execute(query)

        tup = cursor.fetchone()
        if tup is None:
            print 'config data not exist, return None tuple'
        else:
            return {'bl_f1': tup[0],
                    'bl_f2': tup[1],
                    'bl_run_mode': tup[2],
                    'st_press': tup[3],
                    'st_time_limit': tup[4],
                    'st_time_unit': tup[5],
                    'server_addr': tup[6]}
    except Exception as ex:
        print 'exception', ex
    finally:
        db.close()

    return None


def load_device_informations():
    path = os.getenv('ALLUSERSPROFILE')
    if os.path.exists(path + '/SonLaServer'):
        path = path + '/SonLaServer/device_info.inf'
    elif os.path.exists(path + '/Application Data/SonLaServer'):
        path = path + '/Application Data/SonLaServer/device_info.inf'

    if not os.path.isfile(path):
        print 'load device info failed. file not found'
        return None

    result = {}
    with open(path) as file_handle:
        for line in file_handle:
            line = line.replace('\t', '').replace(' ', '')
            device_info = line.split('=')
            if len(device_info) < 2:
                print 'invalid syntax of INF file', line
                continue

            if device_info[0] == 'id_taybac':
                result['id_taybac'] = int(device_info[1])
            elif device_info[0] == 'id_banlay':
                result['id_banlay'] = int(device_info[1])
            elif device_info[0] == 'id_km7':
                result['id_km7'] = int(device_info[1])
            elif device_info[0] == 'id_pidlog1':
                result['id_pidlog1'] = int(device_info[1])
            elif device_info[0] == 'id_pidlog2':
                result['id_pidlog2'] = int(device_info[1])

    return result




