# -*- coding: utf-8 -*-
__author__ = 'Adm'

from PyQt5 import QtCore, QtGui, QtWidgets

from ui.ui_mainwindow1 import Ui_MainWindow1
import res
import defs
import utils
from progresslabel import ProgressLabel


class MainWindow1(QtWidgets.QMainWindow, Ui_MainWindow1):
    def __init__(self, parent=None):
        super(MainWindow1, self).__init__(parent)

        self.setupUi(self)

        self._initialize_widgets()
        self._register_connections()
        self._add_widgets_to_list()

        self.tb_van1_status = False
        self.tb_van2_status = False

        self.timeout_km7 = False
        self.timeout_tb = False
        self.timeout_p2 = False

    def __del__(self):
        pass

    def eventFilter(self, obj, evt):
        """
        :type evt: QtCore.QEvent
        :param obj:
        :param evt:
        :return:
        """
        if isinstance(obj, QtWidgets.QLabel) and evt.type() == QtCore.QEvent.Resize:
            size = QtCore.QSize(obj.width() - 30, obj.height() - 40)
            if obj == self.lbStatusPump1_km7:
                self.movie_p1.setScaledSize(size)

                if self.movie_p1.state() == QtGui.QMovie.Paused:
                    current_frame = self.movie_p1.currentFrameNumber()
                    self.movie_p1.jumpToNextFrame()
                    self.movie_p1.jumpToFrame(current_frame)

            elif obj == self.lbStatusPump2_km7:
                self.movie_p2.setScaledSize(size)

                if self.movie_p2.state() == QtGui.QMovie.Paused:
                    current_frame = self.movie_p2.currentFrameNumber()
                    self.movie_p2.jumpToNextFrame()
                    self.movie_p2.jumpToFrame(current_frame)

        return False

    def update_ui_km7(self, params):
        if not params:
            if not self.timeout_km7:
                for label in self.widgets_km7:
                    label.setText('N/A')

                self.movie_p1.setPaused(True)
                self.movie_p2.setPaused(True)

                self.timeout_km7 = True
            return
        else:
            self.timeout_km7 = False

        self.lbFlow_km7.setText(utils.parse_param(params[defs.ID_KM7_FLOW], 100) + ' m3/h')
        self.lbLevel_km7.setText(utils.parse_param(params[defs.ID_KM7_LEVEL], 1000) + ' m')
        self.lbPressure_km7.setText(utils.parse_param(params[defs.ID_KM7_PRESSURE], 100) + ' kg/cm2')
        self.lbTotalFlow_km7.setText(utils.parse_param(params[defs.ID_KM7_TOTAL_FLOW]) + ' m3')
        self.lbTime1_km7.setText(utils.parse_param(params[defs.ID_KM7_TIME1]) + ' giờ')
        self.lbTime2_km7.setText(utils.parse_param(params[defs.ID_KM7_TIME2]) + ' giờ')

        if int(params[defs.ID_KM7_CONTROL_STATUS]) == defs.CONTROL_MODE_REMOTE:
            self.lbControlStatus_km7.setText(res.STR_REMOTE)
        else:
            self.lbControlStatus_km7.setText(res.STR_LOCAL)

        pump1_status = int(params[defs.ID_KM7_PUMP1])
        pump2_status = int(params[defs.ID_KM7_PUMP2])

        self.movie_p1.setPaused(not pump1_status)
        self.movie_p2.setPaused(not pump2_status)

    def update_ui_taybac(self, params):
        if not params:
            if not self.timeout_tb:
                for label in self.widgets_tb:
                    label.setText('N/A')
                self.btnOnV1_tb.setStyleSheet(res.STYLE_SHEET_BUTTON_NORMAL)
                self.btnOffV1_tb.setStyleSheet(res.STYLE_SHEET_BUTTON_OFF)
                self.timeout_tb = True
            return
        else:
            self.timeout_tb = False
        self.lbTime1_tb.setText(utils.parse_param(params[defs.ID_TB_TIME1]) + ' giờ')

        if int(params[defs.ID_TB_CONTROL_STATUS]) == defs.CONTROL_MODE_REMOTE:
            self.lbControlStatus_tb.setText(res.STR_REMOTE)
        else:
            self.lbControlStatus_tb.setText(res.STR_LOCAL)

        van1_status = int(params[defs.ID_TB_VAN1])
        if van1_status and not self.tb_van1_status:
            self.btnOnV1_tb.setStyleSheet(res.STYLE_SHEET_BUTTON_ON)
            self.btnOffV1_tb.setStyleSheet(res.STYLE_SHEET_BUTTON_NORMAL)
            self.tb_van1_status = True
        elif not van1_status and self.tb_van1_status:
            self.btnOnV1_tb.setStyleSheet(res.STYLE_SHEET_BUTTON_NORMAL)
            self.btnOffV1_tb.setStyleSheet(res.STYLE_SHEET_BUTTON_OFF)
            self.tb_van1_status = False

    def update_ui_p2(self, params):
        if not params:
            if not self.timeout_p2:
                for label in self.widgets_p2:
                    label.setText('N/A')
                self.timeout_p2 = True
            return
        else:
            self.timeout_p2 = False
        self.lbPressure_p2.setText(utils.parse_param(params[defs.ID_PL_PRESSURE], 100) + ' kg/cm2')

    def _initialize_widgets(self):
        self.movie_p1 = QtGui.QMovie(res.URL_PUMP_GIF)
        self.movie_p1.setScaledSize(QtCore.QSize(res.PUMP_GIF_STANDARD_WIDTH,
                                                 res.PUMP_GIF_STANDARD_HEIGHT))
        self.movie_p1.start()
        self.movie_p1.setPaused(True)
        self.lbStatusPump1_km7.setMovie(self.movie_p1)
        self.lbStatusPump1_km7.installEventFilter(self)

        self.movie_p2 = QtGui.QMovie(res.URL_PUMP_GIF)
        self.movie_p2.setScaledSize(QtCore.QSize(res.PUMP_GIF_STANDARD_WIDTH,
                                                 res.PUMP_GIF_STANDARD_HEIGHT))
        self.movie_p2.start()
        self.movie_p2.setPaused(True)
        self.lbStatusPump2_km7.setMovie(self.movie_p2)
        self.lbStatusPump2_km7.installEventFilter(self)

        self.btnOnV1_tb.setStyleSheet(res.STYLE_SHEET_BUTTON_NORMAL)
        self.btnOffV1_tb.setStyleSheet(res.STYLE_SHEET_BUTTON_OFF)

        self.lbProgress_status = ProgressLabel('Getting device sim number')
        self.statusbar.insertPermanentWidget(0, self.lbProgress_status)
        self.lbProgress_status.hide()

    def _register_connections(self):
        pass

    def _add_widgets_to_list(self):
        self.widgets_km7 = [self.lbFlow_km7, self.lbLevel_km7, self.lbPressure_km7,
                            self.lbTotalFlow_km7, self.lbTime2_km7, self.lbTime1_km7,
                            self.lbControlStatus_km7]
        self.widgets_tb = [self.lbControlStatus_tb, self.lbTime1_tb]
        self.widgets_p2 = [self.lbPressure_p2]

    def _update_status_get_sim(self, visible):
        self.lbProgress_status.setVisible(visible)
        if visible:
            self.lbProgress_status.hide_delay(10)



