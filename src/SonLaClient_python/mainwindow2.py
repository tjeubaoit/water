# -*- coding: utf-8 -*-
__author__ = 'Adm'

from PyQt5 import QtCore, QtGui, QtWidgets

from ui.ui_mainwindow2 import Ui_MainWindow2
import res
import defs
import utils
from progresslabel import ProgressLabel


class MainWindow2(QtWidgets.QMainWindow, Ui_MainWindow2):
    def __init__(self, parent=None):
        super(MainWindow2, self).__init__(parent)

        self.setupUi(self)

        self._initialize_widgets()
        self._register_connections()
        self._add_widgets_to_list()

        self.van1_status_bl = False
        self.van2_status_bl = False

        self.timeout_bl = False
        self.timeout_p1 = False

    def __del__(self):
        pass

    def eventFilter(self, obj, evt):
        """
        :type evt: QtCore.QEvent
        :param obj:
        :param evt:
        :return:
        """
        if isinstance(obj, QtWidgets.QLabel) and evt.type() == QtCore.QEvent.Resize:
            size = QtCore.QSize(obj.width() - 40, obj.height())
            if obj == self.lbStatusPump1_bl:
                self.movie_p1.setScaledSize(size)

                if self.movie_p1.state() == QtGui.QMovie.Paused:
                    current_frame = self.movie_p1.currentFrameNumber()
                    self.movie_p1.jumpToNextFrame()
                    self.movie_p1.jumpToFrame(current_frame)

            elif obj == self.lbStatusPump2_bl:
                self.movie_p2.setScaledSize(size)

                if self.movie_p2.state() == QtGui.QMovie.Paused:
                    current_frame = self.movie_p2.currentFrameNumber()
                    self.movie_p2.jumpToNextFrame()
                    self.movie_p2.jumpToFrame(current_frame)

        return False

    def update_ui_banlay(self, params=None):
        if not params:
            if not self.timeout_bl:
                for label in self.widgets_bl:
                    label.setText('N/A')
                self.movie_p1.setPaused(True)
                self.movie_p2.setPaused(True)
                self.btnOnVan1_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_NORMAL)
                self.btnOffVan1_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_OFF)
                self.timeout_bl = True
            return
        else:
            self.timeout_bl = False

        self.lbFlow_bl.setText(utils.parse_param(params[defs.ID_BL_FLOW], 100) + ' m3/h')
        self.lbLevel_bl.setText(utils.parse_param(params[defs.ID_BL_LEVEL], 100) + ' m')
        self.lbPress_bl.setText(utils.parse_param(params[defs.ID_BL_PRESSURE], 100) + ' kg/cm2')
        self.lbTotalFlow_bl.setText(utils.parse_param(params[defs.ID_BL_TOTAL_FLOW]) + ' m3')
        self.lbTime1_bl.setText(utils.parse_param(params[defs.ID_BL_TIME1]) + ' giờ')
        self.lbTime2_bl.setText(utils.parse_param(params[defs.ID_BL_TIME2]) + ' giờ')
        self.lbFrequency1_bl.setText(utils.parse_param(params[defs.ID_FREQUENCY_PUMP1]) + ' Hz')
        self.lbFrequency2_bl.setText(utils.parse_param(params[defs.ID_FREQUENCY_PUMP2]) + ' Hz')

        if int(params[defs.ID_BL_CONTROL_STATUS]) == defs.CONTROL_MODE_REMOTE:
            self.lbControlStatus_bl.setText(res.STR_REMOTE)
        else:
            self.lbControlStatus_bl.setText(res.STR_LOCAL)

        pump1_status = int(params[defs.ID_BL_PUMP1])
        pump2_status = int(params[defs.ID_BL_PUMP2])

        self.movie_p1.setPaused(not pump1_status)
        self.movie_p2.setPaused(not pump2_status)

        van1_status = int(params[defs.ID_BL_VAN1])
        if van1_status and not self.van1_status_bl:
            self.btnOnVan1_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_ON)
            self.btnOffVan1_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_NORMAL)
            self.van1_status_bl = True
        elif not van1_status and self.van1_status_bl:
            self.btnOnVan1_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_NORMAL)
            self.btnOffVan1_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_OFF)
            self.van1_status_bl = False

    def update_run_mode_banlay(self, new_mode):
        if new_mode == defs.RUN_MODE_MANUAL:
            self.btnRunMode_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_MANUAL)
            self.lbRunMode_bl.setText('Chạy tay')

            self.btnOnPump1_bl.setEnabled(True)
            self.btnOffPump1_bl.setEnabled(True)
            self.btnOnPump2_bl.setEnabled(True)
            self.btnOffPump2_bl.setEnabled(True)

            self.gbControlFrequency.setEnabled(True)

        elif new_mode >= defs.RUN_MODE_AUTO:
            self.btnRunMode_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_AUTO)
            self.lbRunMode_bl.setText('Tự động')

            self.btnOnPump1_bl.setEnabled(False)
            self.btnOffPump1_bl.setEnabled(False)
            self.btnOnPump2_bl.setEnabled(False)
            self.btnOffPump2_bl.setEnabled(False)

            self.gbControlFrequency.setEnabled(False)

        if new_mode == defs.RUN_MODE_AUTO_START:
            self.btnAutoRunStart_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_ON)
            self.btnAutoRunStop_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_NORMAL)
            self.btnAutoRunStart_bl.setEnabled(False)

            self.gbSettingAutoRun.setEnabled(False)
            self.gbControlAutoRun.setEnabled(False)

        elif new_mode == defs.RUN_MODE_AUTO_STOP:
            self.btnAutoRunStart_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_NORMAL)
            self.btnAutoRunStop_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_OFF)
            self.btnAutoRunStart_bl.setEnabled(True)

            self.gbSettingAutoRun.setEnabled(True)
            self.gbControlAutoRun.setEnabled(True)

    def update_ui_p1(self, params):
        if not params:
            if not self.timeout_p1:
                for label in self.widget_p1:
                    label.setText('N/A')
                self.timeout_p1 = True
            return
        else:
            self.timeout_p1 = False
        self.lbPress_p1.setText(utils.parse_param(params[defs.ID_PL_PRESSURE], 100) + ' kg/cm2')

    def _initialize_widgets(self):
        self.movie_p1 = QtGui.QMovie(res.URL_PUMP_GIF)
        self.movie_p1.setScaledSize(QtCore.QSize(res.PUMP_GIF_STANDARD_WIDTH,
                                                 res.PUMP_GIF_STANDARD_HEIGHT))
        self.movie_p1.start()
        self.movie_p1.setPaused(True)
        self.lbStatusPump1_bl.setMovie(self.movie_p1)
        self.lbStatusPump1_bl.installEventFilter(self)

        self.movie_p2 = QtGui.QMovie(res.URL_PUMP_GIF)
        self.movie_p2.setScaledSize(QtCore.QSize(res.PUMP_GIF_STANDARD_WIDTH,
                                                 res.PUMP_GIF_STANDARD_HEIGHT))
        self.movie_p2.start()
        self.movie_p2.setPaused(True)
        self.lbStatusPump2_bl.setMovie(self.movie_p2)
        self.lbStatusPump2_bl.installEventFilter(self)

        self.btnOnVan1_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_NORMAL)
        self.btnOffVan1_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_OFF)

        self.btnAutoRunStart_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_NORMAL)
        self.btnAutoRunStop_bl.setStyleSheet(res.STYLE_SHEET_BUTTON_OFF)

        self.lbProgress_status = ProgressLabel('Getting device sim number')
        self.statusbar.insertPermanentWidget(0, self.lbProgress_status)
        self.lbProgress_status.hide()

    def _register_connections(self):
        pass

    def _add_widgets_to_list(self):
        self.widgets_bl = [self.lbFlow_bl, self.lbLevel_bl, self.lbControlStatus_bl,
                           self.lbPress_bl, self.lbTotalFlow_bl, self.lbTime1_bl, self.lbTime2_bl,
                           self.lbFrequency1_bl, self.lbFrequency2_bl]

        self.widget_p1 = [self.lbPress_p1]

    def _update_status_get_sim(self, visible):
        self.lbProgress_status.setVisible(visible)
        if visible:
            self.lbProgress_status.hide_delay(10)
