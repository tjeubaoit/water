# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'reportdialog.ui'
#
# Created: Sat Nov 08 16:13:23 2014
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(811, 510)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setContentsMargins(0, 20, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame = QtWidgets.QFrame(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setMinimumSize(QtCore.QSize(751, 91))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.btnExcel = QtWidgets.QPushButton(self.frame)
        self.btnExcel.setGeometry(QtCore.QRect(470, 10, 101, 23))
        self.btnExcel.setObjectName("btnExcel")
        self.btnExportSelectFinish = QtWidgets.QPushButton(self.frame)
        self.btnExportSelectFinish.setGeometry(QtCore.QRect(690, 59, 81, 23))
        self.btnExportSelectFinish.setObjectName("btnExportSelectFinish")
        self.label_16 = QtWidgets.QLabel(self.frame)
        self.label_16.setGeometry(QtCore.QRect(40, 10, 101, 22))
        self.label_16.setObjectName("label_16")
        self.dtLast = QtWidgets.QDateTimeEdit(self.frame)
        self.dtLast.setGeometry(QtCore.QRect(470, 60, 181, 22))
        self.dtLast.setDate(QtCore.QDate(2000, 2, 24))
        self.dtLast.setCurrentSection(QtWidgets.QDateTimeEdit.DaySection)
        self.dtLast.setCalendarPopup(True)
        self.dtLast.setObjectName("dtLast")
        self.lbDateTimeFirst = QtWidgets.QLabel(self.frame)
        self.lbDateTimeFirst.setGeometry(QtCore.QRect(40, 60, 101, 22))
        self.lbDateTimeFirst.setObjectName("lbDateTimeFirst")
        self.dtFirst = QtWidgets.QDateTimeEdit(self.frame)
        self.dtFirst.setGeometry(QtCore.QRect(140, 60, 171, 22))
        self.dtFirst.setDateTime(QtCore.QDateTime(QtCore.QDate(2014, 3, 9), QtCore.QTime(0, 0, 0)))
        self.dtFirst.setDate(QtCore.QDate(2014, 3, 9))
        self.dtFirst.setCurrentSection(QtWidgets.QDateTimeEdit.DaySection)
        self.dtFirst.setCalendarPopup(True)
        self.dtFirst.setObjectName("dtFirst")
        self.cbDevice = QtWidgets.QComboBox(self.frame)
        self.cbDevice.setGeometry(QtCore.QRect(140, 10, 171, 22))
        self.cbDevice.setObjectName("cbDevice")
        self.lbDateTimeLast = QtWidgets.QLabel(self.frame)
        self.lbDateTimeLast.setGeometry(QtCore.QRect(370, 60, 101, 22))
        self.lbDateTimeLast.setObjectName("lbDateTimeLast")
        self.verticalLayout.addWidget(self.frame)
        self.tableWidget = QtWidgets.QTableWidget(Dialog)
        self.tableWidget.setMinimumSize(QtCore.QSize(0, 329))
        self.tableWidget.setStyleSheet("QTableWidget {\n"
"    margin:20px 20px 0px 20px;\n"
"}")
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.verticalLayout.addWidget(self.tableWidget)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Thống kê"))
        self.btnExcel.setText(_translate("Dialog", "Xuất file Excel"))
        self.btnExportSelectFinish.setText(_translate("Dialog", "Chọn"))
        self.label_16.setText(_translate("Dialog", "Chọn thiết bị"))
        self.dtLast.setDisplayFormat(_translate("Dialog", "dd/MM/yyyy hh:mm"))
        self.lbDateTimeFirst.setText(_translate("Dialog", "Từ ngày"))
        self.dtFirst.setDisplayFormat(_translate("Dialog", "dd/MM/yyyy hh:mm"))
        self.lbDateTimeLast.setText(_translate("Dialog", "Đến ngày"))

