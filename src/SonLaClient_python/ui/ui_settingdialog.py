# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'settingdialog.ui'
#
# Created: Tue Oct 28 17:47:55 2014
# by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(341, 231)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(60, 194, 271, 31))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 321, 179))
        self.groupBox.setStyleSheet("QGroupBox {\n"
                                    "    background-color:#fefefe;\n"
                                    "}")
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(30, 40, 111, 22))
        self.label.setObjectName("label")
        self.editTimeLimit = QtWidgets.QLineEdit(self.groupBox)
        self.editTimeLimit.setGeometry(QtCore.QRect(160, 100, 51, 22))
        self.editTimeLimit.setObjectName("editTimeLimit")
        self.cbTimeUnit = QtWidgets.QComboBox(self.groupBox)
        self.cbTimeUnit.setGeometry(QtCore.QRect(220, 100, 51, 22))
        self.cbTimeUnit.setObjectName("cbTimeUnit")
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setGeometry(QtCore.QRect(30, 100, 111, 22))
        self.label_2.setObjectName("label_2")
        self.spPress = QtWidgets.QDoubleSpinBox(self.groupBox)
        self.spPress.setGeometry(QtCore.QRect(160, 40, 111, 22))
        self.spPress.setObjectName("spPress")

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Thiết lập chạy tự động RTU Bản Lay"))
        self.label.setText(_translate("Dialog", "Áp lực P1"))
        self.editTimeLimit.setText(_translate("Dialog", "0"))
        self.label_2.setText(_translate("Dialog", "Giới hạn thời gian"))
        self.spPress.setSuffix(_translate("Dialog", " kg/cm2"))

