#include "stdafx.h"
#include "BaseThread.h"
#include "Utils.h"


BaseThread::BaseThread()
{
	m_threadHandle = CreateThread(
		NULL,
		0,
		&ThreadImplement,
		(LPVOID) this,
		CREATE_SUSPENDED,
		&m_threadId);
	
	m_quitEvent = CreateEvent(NULL, FALSE, FALSE, 0);

	if (!m_threadHandle || !m_quitEvent)
	{
		WriteLog(GetLastError());
	}
}

BaseThread::~BaseThread()
{
	CloseHandle(m_threadHandle);
	CloseHandle(m_quitEvent);
}

void BaseThread::start()
{
	if (m_threadHandle)
		ResumeThread(m_threadHandle);	
}

void BaseThread::wait(DWORD miliSeconds)
{
	WaitForSingleObject(m_threadHandle, miliSeconds);
	printf("Thread %d has stopped successfully\n", m_threadId);
}

void BaseThread::quit()
{
	SetEvent(m_quitEvent);
}

DWORD WINAPI BaseThread::ThreadImplement(LPVOID obj)
{
	BaseThread *myThread = reinterpret_cast<BaseThread*> (obj);
	return myThread->threadRun();
}

DWORD BaseThread::threadRun()
{
	this->run();
	return 0;
}




