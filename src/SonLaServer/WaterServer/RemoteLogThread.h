#pragma once
#include "BaseThread.h"
#include "Utils.h"

typedef int PERMISSION;
typedef int AUTHEN_STATUS;

typedef struct _REMOTE_TCP_CONNECTION
{
	SOCKET sock;
	SOCKADDR_IN sockAddress;
	PERMISSION permission;
	AUTHEN_STATUS authenStatus;

	BOOL operator ==(const _REMOTE_TCP_CONNECTION &other)
	{
		return this->sock == other.sock;
	}

	BOOL operator !=(const _REMOTE_TCP_CONNECTION &other)
	{
		return this->sock != other.sock;
	}
}
REMOTE_TCP_CONNECTION, *LPREMOTE_TCP_CONNECTION;


typedef struct _REMOTE_BUFFER
{
	CHAR buff[MAX_BUFFER_SIZE];
	DWORD dwSize;
	CRITICAL_SECTION criticalSection;
	HANDLE newDataEvent;
}
REMOTE_BUFFER, *LPREMOTE_BUFFER;


class RemoteLogThread : public BaseThread
{
public:
	RemoteLogThread();
	virtual ~RemoteLogThread();

	BOOL init();

	VOID setNewData(DWORD buff_len, PCHAR buffer);

	BOOL hasControlCmdFromClient();
	BOOL getLastControlData(PCHAR buff, DWORD max_len, PDWORD nBytesRead);

protected:
	virtual void run();

private:
	HANDLE m_fileHandle;
	REMOTE_TCP_CONNECTION m_server;

	REMOTE_BUFFER m_outBuff;
	REMOTE_BUFFER m_inBuff;

	BOOL readData(REMOTE_TCP_CONNECTION *con);
	BOOL writeData(REMOTE_TCP_CONNECTION *con, DWORD len, PCHAR buff);
	BOOL writeDataToLogFile(DWORD len, LPTSTR buff);
};



