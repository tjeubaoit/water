#pragma once
#include "BaseThread.h"

typedef struct _MY_SEMAPHORE
{
	DWORD dwCount;
	DWORD dwPosInBuff;
	CRITICAL_SECTION criticalSection;
	HANDLE enableEvent;
}
MY_SEMAPHORE, *PMY_SEMAPHORE, *LPMY_SEMAPHORE;


class SqlLiteThread : public BaseThread
{
public:
	SqlLiteThread();
	virtual ~SqlLiteThread();

	VOID setNewData(DWORD buff_len, PCHAR buff);

protected:
	VOID run();

private:
	MY_SEMAPHORE m_inSem;
	MY_SEMAPHORE m_outSem;

	CHAR m_buffer[MAX_BUFFER_SIZE];

	BOOL acquireSemaphore(LPMY_SEMAPHORE semaphore, DWORD count, DWORD timeout);
	VOID releaseSemaphore(LPMY_SEMAPHORE semaphore, DWORD count);
};

