__author__ = 'Adm'

import sqlite3
import os
import datetime


path = os.getenv('ALLUSERSPROFILE') 
if os.path.exists(path + '/Application Data'):
    path = path + '/Application Data/SonLaServer'
else:
    path = path + '/SonLaServer'

ID_KM7 = 401
ID_BANLAY = 402
ID_TAYBAC = 403
ID_P1 = 404
ID_P2 = 405


def print_error(log_file_path, errmsg):
    error_file = open(log_file_path + '/db_error.log', 'a')
    if error_file:
        current_time_str = datetime.datetime.now().strftime('[%Y-%m-%d %H:%M:%S] ')
        error_file.write(current_time_str + errmsg + '\r\n')
        print errmsg


def load_device_informations():
    global ID_KM7
    global ID_BANLAY
    global ID_TAYBAC
    global ID_P1
    global ID_P2

    path = os.getenv('ALLUSERSPROFILE')
    if os.path.exists(path + '/SonLaServer'):
        path = path + '/SonLaServer/device_info.inf'
    elif os.path.exists(path + '/Application Data/SonLaServer'):
        path = path + '/Application Data/SonLaServer/device_info.inf'

    if not os.path.isfile(path):
        print 'load device info failed. file not found'
        return None

    result = {}
    with open(path) as file_handle:
        for line in file_handle:
            line = line.replace('\t', '').replace(' ', '')
            device_info = line.split('=')
            if len(device_info) < 2:
                print 'invalid syntax of INF file', line
                continue

            if device_info[0] == 'id_taybac':
                ID_TAYBAC = int(device_info[1])
            elif device_info[0] == 'id_banlay':
                ID_BANLAY = int(device_info[1])
            elif device_info[0] == 'id_km7':
                ID_KM7 = int(device_info[1])
            elif device_info[0] == 'id_pidlog1':
                ID_P1 = int(device_info[1])
            elif device_info[0] == 'id_pidlog2':
                ID_P2 = int(device_info[1])


def get_insert_query(device_id):
    insert_query = None

    if device_id == ID_KM7:
        insert_query = '''INSERT INTO km7 (timeserver, signal, volt, press, level, flow,
                        pump1, pump2, control_state, time1, total_flow, time2)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
    elif device_id == ID_BANLAY:
        insert_query = '''INSERT INTO banlay (timeserver, signal, volt, press, level, flow,
                        pump1, pump2, van1, van2, control_state, time1, total_flow, time2,
                        frequency1, frequency2)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
    elif device_id == ID_TAYBAC:
        insert_query = '''INSERT INTO taybac (timeserver, signal, volt,
                        van1, van2, control_state, time1, time2)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?)'''
    elif device_id == ID_P1:
        insert_query = '''INSERT INTO pidlog1 (timeserver, press, signal, volt)
                        VALUES (?, ?, ?, ?)'''
    elif device_id == ID_P2:
        insert_query = '''INSERT INTO pidlog2 (timeserver, press, signal, volt)
                        VALUES (?, ?, ?, ?)'''

    return insert_query


def insert_db(data):
    """
    :param data: str
    :return: int
    """
    load_device_informations()

    try:
        data_array = data.split(',')
        device_id = int(data_array[0])
        data_array[0] = datetime.datetime.now().isoformat(' ')

        db_name = path + '/sonla.db'
        con = sqlite3.connect(db_name)
        cursor = con.cursor()

        query = get_insert_query(device_id)
        if query:
            if device_id == ID_P1 or device_id == ID_P2:
                data_array[4] = str(float(data_array[4]) / 10)
                cursor.execute(query, (data_array[0], data_array[1],
                                       data_array[7], data_array[4]))
            else:
                data_array[2] = str(float(data_array[2]) / 10)
                cursor.execute(query, tuple(data_array))
            con.commit()

        con.close()

    except Exception as ex:
        errmsg = 'Exception when insert to db. ' + ex.message
        print_error(path, ex.message)


def create_db():
    if not os.path.exists(path):
        os.makedirs(path)
        print_error(path, 'path to save db data not exist, create directory')        

    file_name = path + '/sonla.db'
    if not os.path.isfile(file_name):
        print_error(path, 'db file not exist, create new')
        open(file_name, 'a').close()


def create_table():
    dbname = path + '/sonla.db'
    try:
        con = sqlite3.connect(dbname)
        cursor = con.cursor()
        cursor.executescript('''
                    CREATE TABLE IF NOT EXISTS km7 (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        timeserver text,
                        signal INTEGER,
                        volt REAL,
                        press REAL,
                        level REAL,
                        flow REAL,
                        pump1 INTEGER,
                        pump2 INTEGER,
                        control_state INTEGER,
                        time1 INTEGER,
                        time2 INTEGER,
                        total_flow REAL
                    );

                    CREATE TABLE IF NOT EXISTS banlay (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        timeserver text,
						signal INTEGER,
						volt REAL,
                        press REAL,
                        level REAL,
                        flow REAL,
                        pump1 INTEGER,
                        pump2 INTEGER,
                        van1 INTEGER,
						van2 INTEGER,
						control_state INTEGER,
                        time1 INTEGER,
						time2 INTEGER,
                        total_flow REAL,
                        frequency1 INTEGER,
                        frequency2 INTEGER
                    );

                    CREATE TABLE IF NOT EXISTS taybac (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        timeserver text,
						signal INTEGER,
						volt REAL,
                        van1 INTEGER,
						van2 INTEGER,
						control_state INTEGER,
						time1 INTEGER,
						time2 INTEGER
                    );

                    CREATE TABLE IF NOT EXISTS pidlog1 (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        timeserver text,
                        press REAL,
						signal INTEGER,
						volt REAL
                    );

                    CREATE TABLE IF NOT EXISTS pidlog2 (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        timeserver text,
                        press REAL,
						signal INTEGER,
						volt REAL
                    );
        ''')

        con.commit()
        con.close()

    except Exception as ex:
        errmsg = 'Exception when excute query create table. ' + ex.message
        print_error(path, errmsg)
