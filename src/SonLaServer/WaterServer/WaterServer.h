#ifndef WATERSERVER_H
#define WATERSERVER_H

#include <WinSock2.h>

typedef struct _TCP_CONNECTION
{
	SOCKET sock;
	SOCKADDR_IN sockAddress;
	SYSTEMTIME lastTimeSendData;
	int deviceId;
} 
TCP_CONNECTION, *PTCP_CONNECTION, *LPTCP_CONNECTION;


typedef struct _SERVER_STATUS
{
	BOOL blDnsSocketCreatedSuccess;
	DWORD dwDnsServerStatus;
	BOOL blListening;
	BOOL blLogThreadRunning;
} 
SERVER_STATUS, *PSERVER_STATUS, *LPSERVER_STATUS;


DWORD WINAPI ServiceCtrlHandlerEx(DWORD controlCode, DWORD eventType, VOID *eventData, VOID *context);
VOID WINAPI ServiceMain(DWORD argc, LPTSTR *argv);

VOID InstallService();
BOOL StartMyService();
BOOL StopMyService(SC_HANDLE service);
VOID UninstallService();
VOID RunService();

VOID MainTask();
VOID InitService();
VOID InitDnsServer(BOOL isFirstTime = FALSE);
VOID CleanService();

BOOL OnConnect();
BOOL OnClose(LPTCP_CONNECTION &con);
BOOL OnError(LPTCP_CONNECTION &con);
BOOL OnRead(LPTCP_CONNECTION &con);
BOOL OnWrite(LPTCP_CONNECTION &con, PCHAR buff, DWORD buff_len);

VOID CheckKeepDnsConnection();
VOID CheckHasControlFromRemote();
BOOL CheckDeviceNotSendDataTimeout(LPTCP_CONNECTION &con);

VOID InitFdSet(fd_set &fdread, fd_set &fdwrite, fd_set &fderror);

#define DESTROY_DNS_CONNECTION(con) \
	shutdown(con->sock, SD_BOTH); \
	closesocket(con->sock); \
	con->sock = INVALID_SOCKET;

#endif // end WATERSERVER_H


