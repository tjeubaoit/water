#ifndef BASETHREAD_H
#define BASETHREAD_H

#include <WinSock2.h>

#define MAX_BUFFER_SIZE 4096

class BaseThread
{
public:
	BaseThread();
	virtual ~BaseThread();
	
	void start();
	void wait(DWORD miliSeconds = INFINITE);
	void quit();

	virtual void setNewData(DWORD buff_len, PCHAR buffer) = 0;

protected:
	virtual void run() = 0;

	HANDLE m_threadHandle;
	DWORD m_threadId;
	HANDLE m_quitEvent;

private:
	static DWORD WINAPI ThreadImplement(LPVOID obj);
	DWORD threadRun();
};

#endif