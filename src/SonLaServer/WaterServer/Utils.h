#pragma once
#include <WinSock2.h>

#define CONNECTED 1
#define DISCONNECTED 0

typedef BOOL SOCKET_STATE;

VOID WriteLog(DWORD enumber, LPTSTR lpMsgBuf = NULL);

VOID WriteTCharToFile(HANDLE hFile, size_t len, LPTSTR msgBuf, BOOL closeAfterFinish);

VOID PrintSocketInfo(const SOCKADDR_IN &sockAddr, SOCKET_STATE sockState, USHORT portConnected = 0);

BOOL CheckTimeOut(const LPSYSTEMTIME startTime, const LPSYSTEMTIME endTime, DWORD timeout);

BOOL LoadDeviceInformation(int *ids);
BOOL SaveDeviceInfomartion(int *ids);

#define DESTROY_CONNECTION(con) \
	shutdown(con->sock, SD_BOTH); \
	closesocket(con->sock); \
	delete con;