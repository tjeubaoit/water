#include "stdafx.h"
#include "RemoteLogThread.h"
#include "Utils.h"
#include <iostream>
#include <list>
#include <algorithm>
#include <stdio.h>
#include <Ws2tcpip.h>
#include <ShlObj.h>
#include <Shlwapi.h>
#include <assert.h>

#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "Shlwapi.lib")

#define PORT 6969
#define BACKLOG 10
#define MAX_NAME 128

#define PERMISSION_GUEST 0
#define PERMISSION_READ 1
#define PERMISSION_WRITE 2

#define PERMISSION_READ_PASS	"vtekremote"
#define PERMISSION_WRITE_PASS	"vtekadmin"

#define AUTHEN_START 3
#define AUTHEN_FAIL 0


RemoteLogThread::RemoteLogThread() 
	: BaseThread()
{
	ZeroMemory(&m_inBuff, sizeof(m_inBuff));
	ZeroMemory(&m_outBuff, sizeof(m_outBuff));

	InitializeCriticalSection(&m_inBuff.criticalSection);
	InitializeCriticalSection(&m_outBuff.criticalSection);

	m_inBuff.newDataEvent = CreateEvent(NULL, FALSE, FALSE, 0);
	m_outBuff.newDataEvent = CreateEvent(NULL, FALSE, FALSE, 0);

	assert(m_inBuff.newDataEvent);
	assert(m_outBuff.newDataEvent);

	SYSTEMTIME time;
	TCHAR fileName[MAX_NAME];
	TCHAR filePath[MAX_PATH];

#ifdef BUILD_AS_PROGRAM
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, filePath)))
#else
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, filePath)))
#endif // BUILD_AS_PROGRAM
	{
		_tcscat_s(filePath, _T("\\SonLaServer"));
		if (!PathFileExists(filePath)) 
			CreateDirectory(filePath, NULL);

		_tcscat_s(filePath, _T("\\logs"));
		if (!PathFileExists(filePath)) 
			CreateDirectory(filePath, NULL);

		ZeroMemory(fileName, MAX_NAME);
		GetLocalTime(&time);
		_stprintf_s(fileName, _T("\\sonlaserver_%d%d%d_%d%d%d.log"),
			time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);
		_tcscat_s(filePath, fileName);

		m_fileHandle = CreateFile(
			filePath, 
			GENERIC_WRITE, 
			FILE_SHARE_WRITE, 
			NULL, CREATE_ALWAYS, 
			FILE_ATTRIBUTE_NORMAL, 
			NULL);
	}
	else
	{
		printf("SHgetfolder error\n");
		return;
	}
}

RemoteLogThread::~RemoteLogThread()
{
	DeleteCriticalSection(&m_inBuff.criticalSection);
	DeleteCriticalSection(&m_outBuff.criticalSection);

	CloseHandle(m_inBuff.newDataEvent);
	CloseHandle(m_outBuff.newDataEvent);
	CloseHandle(m_fileHandle);
}

void RemoteLogThread::run()
{
	TIMEVAL timeVal = { 0, 100000 };
	fd_set fdread, fderror;
	std::list<LPREMOTE_TCP_CONNECTION> connected_list, closed_list;

	connected_list.push_back(&m_server);

	while (WaitForSingleObject(m_quitEvent, 0) == WAIT_TIMEOUT)
	{
		FD_ZERO(&fdread);
		FD_ZERO(&fderror);
		FD_SET(m_server.sock, &fdread);

		for each (LPREMOTE_TCP_CONNECTION con in connected_list)
		{
			if (con != &m_server)
			{
				FD_SET(con->sock, &fdread);
				FD_SET(con->sock, &fderror);
			}
		}

		if (WaitForSingleObject(m_outBuff.newDataEvent, 0) == WAIT_OBJECT_0)
		{
			EnterCriticalSection(&m_outBuff.criticalSection);
			if (m_outBuff.dwSize > 0)
			{
#ifdef UNICODE
				MultiByteToWideChar(0, 0, NULL, 0, NULL, 0)
				this->writeDataToLogFile(m_outBuff.dwSize, NULL);
#else
				this->writeDataToLogFile(m_outBuff.dwSize, m_outBuff.buff);
#endif
				for each (LPREMOTE_TCP_CONNECTION con in connected_list)
				{
					if (con != &m_server && con->permission >= PERMISSION_READ)
						writeData(con, m_outBuff.dwSize, m_outBuff.buff);
				}
				m_outBuff.dwSize = 0;
			}
			LeaveCriticalSection(&m_outBuff.criticalSection);
		}

		if (select(0, &fdread, NULL, &fderror, &timeVal) == SOCKET_ERROR)
		{
			WriteLog(WSAGetLastError());
			continue;
		}

		for each (LPREMOTE_TCP_CONNECTION con in connected_list)
		{
			if (FD_ISSET(con->sock, &fdread))
			{
				if (con == &m_server) // new request connection
				{
					LPREMOTE_TCP_CONNECTION client = new REMOTE_TCP_CONNECTION;
					SOCKADDR_IN clientAddr;
					int len = sizeof(clientAddr);

					client->sock = accept(m_server.sock, (SOCKADDR *)&clientAddr, &len);
					if (client->sock == INVALID_SOCKET)
					{
						delete con;
						continue;
					}
					ULONG ul = 1;
					if (ioctlsocket(client->sock, FIONBIO, (PULONG)&ul) == SOCKET_ERROR)
					{
						delete con;
						continue;
					}

					if (clientAddr.sin_addr.s_addr == inet_addr("127.0.0.1"))
						client->permission = PERMISSION_WRITE;
					else
						client->permission = PERMISSION_GUEST;

					client->sockAddress = clientAddr;
					client->authenStatus = AUTHEN_START;
					if (client->permission == PERMISSION_GUEST)
					{
						CHAR welcome[] = "Enter password to login\n";
						send(client->sock, welcome, sizeof(welcome), 0);
					}

					connected_list.push_back(client);
					PrintSocketInfo(clientAddr, CONNECTED, PORT);
				}
				else if (!readData(con))
				{
					closed_list.push_back(con);
					PrintSocketInfo(con->sockAddress, DISCONNECTED);
				}
			}
			if (FD_ISSET(con->sock, &fderror))
			{
				if (std::find(closed_list.begin(), closed_list.end(), con) != closed_list.end())
				{
					closed_list.push_back(con);
					WriteLog(WSAGetLastError());
				}
			}
		}

		for each (LPREMOTE_TCP_CONNECTION con in closed_list)
		{
			connected_list.remove(con);
			DESTROY_CONNECTION(con);
		}
		closed_list.clear();
	}

	for each (LPREMOTE_TCP_CONNECTION con in connected_list)
	{
		DESTROY_CONNECTION(con);
	}
}

BOOL RemoteLogThread::init()
{
	m_server.sockAddress.sin_family = AF_INET;
	m_server.sockAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	m_server.sockAddress.sin_port = htons(PORT);

	m_server.sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	ULONG ul = 1;
	if (ioctlsocket(m_server.sock, FIONBIO, (PULONG)&ul) == SOCKET_ERROR)
	{
		WriteLog(WSAGetLastError());
		return FALSE;
	}
	if (bind(m_server.sock, (SOCKADDR *)&m_server.sockAddress, sizeof(SOCKADDR_IN)) == SOCKET_ERROR)
	{
		WriteLog(WSAGetLastError());
		return FALSE;
	}
	if (listen(m_server.sock, BACKLOG) == SOCKET_ERROR)
	{
		WriteLog(WSAGetLastError());
		return FALSE;
	}

	return TRUE;
}

VOID RemoteLogThread::setNewData(DWORD buff_len, PCHAR buffer)
{
	EnterCriticalSection(&m_outBuff.criticalSection);

	ZeroMemory(m_outBuff.buff, MAX_BUFFER_SIZE);
	memcpy_s(m_outBuff.buff, MAX_BUFFER_SIZE, buffer, buff_len);
	m_outBuff.dwSize = buff_len;

	LeaveCriticalSection(&m_outBuff.criticalSection);
	SetEvent(m_outBuff.newDataEvent);
}

BOOL RemoteLogThread::readData(LPREMOTE_TCP_CONNECTION con)
{
	EnterCriticalSection(&m_inBuff.criticalSection);

	ZeroMemory(m_inBuff.buff, MAX_BUFFER_SIZE);
	m_inBuff.dwSize = recv(con->sock, m_inBuff.buff, sizeof(m_inBuff), 0);	

	LeaveCriticalSection(&m_inBuff.criticalSection);

	if (m_inBuff.dwSize == SOCKET_ERROR)
	{
		WriteLog(WSAGetLastError());
		return WSAGetLastError() == WSAEWOULDBLOCK;
	}
	else if (m_inBuff.dwSize > 0)
	{
		if (con->permission == PERMISSION_GUEST)
		{
			TCHAR tmp[256] = { 0 };
			size_t tmpSize = 256;
			CHAR *ipAddr = inet_ntoa(con->sockAddress.sin_addr);	
			const WORD port = con->sockAddress.sin_port;

			if (!strcmp(m_inBuff.buff, PERMISSION_READ_PASS))
			{
				CHAR result[] = "Login success with read permission\n";
				send(con->sock, result, sizeof(result), 0);

				con->permission = PERMISSION_READ;
				_stprintf_s(tmp, _T("Remote client at %s:%d login with read permission\n"), 
					ipAddr, port);
				WriteLog(0, tmp);
			}
			else if (!strcmp(m_inBuff.buff, PERMISSION_WRITE_PASS))
			{
				CHAR result[] = "Login success with write permission\n";
				send(con->sock, result, sizeof(result), 0);

				con->permission = PERMISSION_WRITE;
				_stprintf_s(tmp, _T("Remote client %s:%d login with write permission\n"), 
					ipAddr, port);
				WriteLog(0, tmp);
			}
			else
			{
				CHAR loginFailText[] = "Login failed, try again\n";
				CHAR closeSockText[] = "Login failed 3 times, closed socket. Bye bye\n";

				con->authenStatus = con->authenStatus - 1;
				send(con->sock, loginFailText, sizeof(loginFailText), 0);

				_stprintf_s(tmp, _T("Remote client %s:%d login fail, data sent: %s\n"), 
					ipAddr, port, m_inBuff.buff);
				WriteLog(0, tmp);

				if (con->authenStatus <= AUTHEN_FAIL)
				{
					send(con->sock, closeSockText, sizeof(closeSockText), 0);

					ZeroMemory(tmp, sizeof(tmp));
					_stprintf_s(tmp, _T("Disconnect with remote client %s:%d because login fail\n"), 
						ipAddr, port);
					WriteLog(0, tmp);
					
					return FALSE;
				}
			}
			return TRUE;
		}
		else if (con->permission == PERMISSION_WRITE)
		{
			TCHAR buff[1024];
			_stprintf_s(buff, _T("New data from remote client: %s\n"), m_inBuff.buff);
			WriteLog(0, buff);
			SetEvent(m_inBuff.newDataEvent);
		}
	}
	return m_inBuff.dwSize;
}

BOOL RemoteLogThread::writeData(LPREMOTE_TCP_CONNECTION con, DWORD len, PCHAR buff)
{
	int nBytes = send(con->sock, buff, len, 0);
	if (nBytes == SOCKET_ERROR)
	{		
		WriteLog(WSAGetLastError());
		return WSAGetLastError() == WSAEWOULDBLOCK;
	}
	return nBytes;
}

BOOL RemoteLogThread::writeDataToLogFile(DWORD len, LPTSTR buff)
{
	if (m_fileHandle != INVALID_HANDLE_VALUE)
	{
		WriteTCharToFile(m_fileHandle, len, buff, FALSE);
		return TRUE;
	}
	return FALSE;
}

BOOL RemoteLogThread::hasControlCmdFromClient()
{
	return WaitForSingleObject(m_inBuff.newDataEvent, 0) == WAIT_OBJECT_0;
}

BOOL RemoteLogThread::getLastControlData(PCHAR buff, DWORD max_len, PDWORD nBytesRead)
{
	if (m_inBuff.dwSize <= 0)
		return FALSE;

	EnterCriticalSection(&m_inBuff.criticalSection);

	ZeroMemory(buff, max_len);
	*nBytesRead = (m_inBuff.dwSize < max_len) ? m_inBuff.dwSize : max_len;	
	memcpy_s(buff, max_len, m_inBuff.buff, *nBytesRead);
	m_inBuff.dwSize = 0; // reset

	LeaveCriticalSection(&m_inBuff.criticalSection);

	return TRUE;
}
