#include "stdafx.h"
#include "SqlLiteThread.h"
#include "Utils.h"

#ifndef _DEBUG
#	include <Python.h>
#endif

#include <string>
#include <Strsafe.h>
#include <assert.h>

#pragma comment (lib, "Shlwapi.lib")

#define MAX_SEM_COUNT MAX_BUFFER_SIZE
#define MAX_WAIT 1000
#define MIN_MESSAGE_SIZE 51

#define Py_PROGRAM_NAME			"SonLaServer_ExPyModule"
#define Py_MODULE_NAME			"db_functions"
#define Py_FUNC_INSERT			"insert_db"
#define Py_FUNC_CREATE_TABLE	"create_table"
#define Py_FUNC_CREATE_DB		"create_db"


SqlLiteThread::SqlLiteThread() : BaseThread()
{
	ZeroMemory(&m_inSem, sizeof(m_inSem));
	ZeroMemory(&m_outSem, sizeof(m_outSem));

	InitializeCriticalSection(&m_inSem.criticalSection);
	InitializeCriticalSection(&m_outSem.criticalSection);

	m_inSem.dwCount = MAX_SEM_COUNT;
	m_inSem.enableEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	m_outSem.dwCount = 0;
	m_outSem.enableEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	assert(m_inSem.enableEvent);
	assert(m_outSem.enableEvent);
}

SqlLiteThread::~SqlLiteThread()
{
	DeleteCriticalSection(&m_inSem.criticalSection);
	DeleteCriticalSection(&m_outSem.criticalSection);

	CloseHandle(m_inSem.enableEvent);
	CloseHandle(m_outSem.enableEvent);
}

VOID SqlLiteThread::setNewData(DWORD buff_len, PCHAR buff)
{
	if (!acquireSemaphore(&m_inSem, buff_len, MAX_WAIT))
	{
		WriteLog(0, _T("Cannot acquire semaphore, buffer is full\n"));
		return;
	}

	DWORD nBytesFree = MAX_BUFFER_SIZE - m_inSem.dwPosInBuff;
	if (m_inSem.dwPosInBuff + buff_len > MAX_BUFFER_SIZE)
	{
		DWORD leftBytes = buff_len - nBytesFree;
		memcpy_s(m_buffer + m_inSem.dwPosInBuff, nBytesFree, buff, nBytesFree);
		memcpy_s(m_buffer, MAX_BUFFER_SIZE, buff + nBytesFree, leftBytes);
		m_inSem.dwPosInBuff = leftBytes;
	}
	else
	{
		memcpy_s(m_buffer + m_inSem.dwPosInBuff, nBytesFree, buff, buff_len);
		m_inSem.dwPosInBuff += buff_len;
	}

	releaseSemaphore(&m_outSem, buff_len);
	if (m_outSem.dwCount >= MIN_MESSAGE_SIZE)
		SetEvent(m_outSem.enableEvent);
}

VOID SqlLiteThread::run()
{
#ifndef _DEBUG
	PyObject *pName, *pModule, *pFunc, *pArgs;

	Py_SetProgramName(Py_PROGRAM_NAME);
	Py_Initialize();

	pName = PyString_FromString(Py_MODULE_NAME);
	pModule = PyImport_Import(pName);
	assert(pModule);

	pFunc = PyObject_GetAttrString(pModule, Py_FUNC_CREATE_DB);
	assert(pFunc && PyCallable_Check(pFunc));
	PyObject_CallObject(pFunc, NULL);
	Py_DECREF(pFunc);

	pFunc = PyObject_GetAttrString(pModule, Py_FUNC_CREATE_TABLE);
	assert(pFunc && PyCallable_Check(pFunc));
	PyObject_CallObject(pFunc, NULL);
	Py_DECREF(pFunc);

	pFunc = PyObject_GetAttrString(pModule, Py_FUNC_INSERT);
	assert(pFunc != NULL && PyCallable_Check(pFunc));
#endif // !_DEBUG

	std::string str;
	CHAR tmpBuff[256];

	while (WaitForSingleObject(m_quitEvent, 0) == WAIT_TIMEOUT)
	{
		if (!acquireSemaphore(&m_outSem, MIN_MESSAGE_SIZE, MAX_WAIT))
			continue;

		DWORD nBytesLeft;
		if ((nBytesLeft = MAX_BUFFER_SIZE - m_outSem.dwPosInBuff) < MIN_MESSAGE_SIZE)
		{
			str.append(m_buffer + m_outSem.dwPosInBuff, nBytesLeft);
			str.append(m_buffer, MIN_MESSAGE_SIZE - nBytesLeft);
			m_outSem.dwPosInBuff = MIN_MESSAGE_SIZE - nBytesLeft;
		}
		else
		{
			str.append(m_buffer + m_outSem.dwPosInBuff, MIN_MESSAGE_SIZE);
			m_outSem.dwPosInBuff += MIN_MESSAGE_SIZE;
		}

		releaseSemaphore(&m_inSem, MIN_MESSAGE_SIZE);
		SetEvent(m_inSem.enableEvent);

		std::size_t pos = str.find('\n');
		if (std::string::npos == pos)
		{
#ifdef BUILD_AS_PROGRAM
			DWORD size = str.size() + 100;
			TCHAR *tmp = new TCHAR[size];

			StringCchPrintf(tmp, size,
				_T("Not found newline character, current data: %s\n"), str.c_str());
			WriteLog(0, tmp);
			delete[] tmp;
#endif
			continue;
		}

		ZeroMemory(tmpBuff, sizeof(tmpBuff));
		memcpy_s(tmpBuff, sizeof(tmpBuff), str.c_str(), pos);
		str.erase(str.begin(), str.begin() + pos + 1);
#ifndef _DEBUG
		pArgs = PyTuple_Pack(1, PyString_FromString(tmpBuff));
		if (pArgs)
		{
			PyObject_CallObject(pFunc, pArgs);
			Py_DECREF(pArgs);
		}
#else
		printf("Tuple data %s\n", tmpBuff);
#endif
	}
#ifndef _DEBUG
	Py_DECREF(pName);
	Py_DECREF(pModule);

	Py_Finalize();
#endif // !_DEBUG
}

BOOL SqlLiteThread::acquireSemaphore(LPMY_SEMAPHORE semaphore, DWORD count, DWORD timeout)
{
	SYSTEMTIME startTime, endTime;
	GetLocalTime(&startTime);
	BOOL bOk = FALSE;
	do 
	{
		EnterCriticalSection(&semaphore->criticalSection);
		if (semaphore->dwCount >= count || count == 0)
		{
			bOk = TRUE;
			semaphore->dwCount = (count > 0) ? (semaphore->dwCount - count) : 0;
#ifdef BUILD_AS_PROGRAM
			CHAR *tmp = (semaphore == &m_inSem) ? "IN" : "OUT";
			printf("Acquire semaphore %s, count: %d\n", tmp, semaphore->dwCount);
#endif // BUILD_AS_PROGRAM

			LeaveCriticalSection(&semaphore->criticalSection);
			break;
		}
		else
		{
			LeaveCriticalSection(&semaphore->criticalSection);
			GetLocalTime(&endTime);
			continue;
		}
	} while (WaitForSingleObject(semaphore->enableEvent, timeout) == WAIT_OBJECT_0);

	return bOk;
}

VOID SqlLiteThread::releaseSemaphore(LPMY_SEMAPHORE semaphore, DWORD count)
{
	EnterCriticalSection(&semaphore->criticalSection);

	semaphore->dwCount += count;
#ifdef BUILD_AS_PROGRAM
	CHAR *tmp = (semaphore == &m_inSem) ? "IN" : "OUT";
	printf("Release semaphore %s, count: %d\n", tmp, semaphore->dwCount);
#endif // BUILD_AS_PROGRAM

	LeaveCriticalSection(&semaphore->criticalSection);
}


