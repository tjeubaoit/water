// WaterServer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#ifdef _DEBUG
#include <vld.h>
#endif
#include "WaterServer.h"
#include "RemoteLogThread.h"
#include "SqlLiteThread.h"
#include "Utils.h"
#include <Ws2tcpip.h>
#include <stdio.h>
#include <list>
#include <algorithm>
#include <assert.h>
#include <stdlib.h>

#pragma comment(lib, "Ws2_32.lib")

#define SERVICE_NAME					_T("SonLaServer")
#define SERVICE_DISPLAY_NAME			_T("SonLa Water Server")

#define SERVER_PORT						9999
#define BACKLOG							10
#define READ_BUFFER_SIZE				2048

#define DNS_SERVER_ADDR					"112.213.91.249"
#define DNS_SERVER_PORT					9999

#define DNS_DATA_SENT					"400,0\n"
#define DNS_TIMER_INTERVAL				30000 // 30 seconds
#define DNS_WAITING_TIMER_INTERVAL		-200000000LL // x 100 nano seconds = 20 seconds

#define DNS_STATUS_CONNECTING			1
#define DNS_STATUS_CONNECTED			1000
#define DNS_STATUS_DISCONECTED			0

#define DEVICE_NOT_SEND_DATA_TIMEOUT	1200000000LL // 120 seconds

#define INVALID_DEVICE_ID -1

int g_deviceId[] = { 401, 402, 403, 404, 405 };

SERVICE_STATUS					m_serviceStatus;
SERVICE_STATUS_HANDLE			m_serviceStatusHandle = 0;
HANDLE							m_stopServiceEvent = 0;

TCP_CONNECTION					m_server;
SERVER_STATUS					m_serverStatus;
std::list<LPTCP_CONNECTION>		m_tcpConList, m_tcpClosedList;

TCP_CONNECTION					m_dnsCon;
HANDLE							m_dnsTimer = INVALID_HANDLE_VALUE;
HANDLE							m_dnsWaitTimeoutTimer = INVALID_HANDLE_VALUE;

RemoteLogThread					*m_remoteLogThread;
SqlLiteThread					*m_sqlLiteThread;


int _tmain(int argc, _TCHAR* argv[])
{
	OSVERSIONINFOEX osVersionInfo;
	DWORDLONG dwlConditionMask = 0;

	ZeroMemory(&osVersionInfo, sizeof(OSVERSIONINFOEX));
	osVersionInfo.dwOSVersionInfoSize = sizeof(osVersionInfo);
	osVersionInfo.dwPlatformId = VER_PLATFORM_WIN32_NT;

	VER_SET_CONDITION(dwlConditionMask, VER_PLATFORMID, VER_GREATER_EQUAL);

	if (VerifyVersionInfo(&osVersionInfo, VER_PLATFORMID, dwlConditionMask))
	{
		if (argc > 1 && lstrcmpi(argv[1], TEXT("install")) == 0)
		{
			printf("Installing Service ...\n");
			InstallService();
		}
		else if (argc > 1 && lstrcmpi(argv[1], TEXT("delete")) == 0)
		{
			printf("Uninstalling Service ...\n");
			UninstallService();
		}
		else if (argc > 1 && lstrcmpi(argv[1], TEXT("start")) == 0)
		{
			printf("Starting Service ...\n");
			StartMyService();
		}
		else if (argc > 1 && lstrcmpi(argv[1], TEXT("stop")) == 0)
		{
			printf("Stopping Service ...\n");
			StopMyService(0);
		}
		else
#ifdef BUILD_AS_PROGRAM
			MainTask();
#else
			RunService();
#endif
	}
	else
	{
		printf("OS version not support for using service\n");
		system("pause");
		return 1;		
	}

	return 0;
}

DWORD WINAPI ServiceCtrlHandlerEx(DWORD controlCode, DWORD eventType, VOID *eventData, VOID *context)
{
	switch (controlCode)
	{
	case SERVICE_CONTROL_INTERROGATE:
		break;

	case SERVICE_CONTROL_SHUTDOWN:
	case SERVICE_CONTROL_STOP:
		m_serviceStatus.dwCurrentState = SERVICE_STOP_PENDING;
		m_serviceStatus.dwWaitHint = 20000;
		m_serviceStatus.dwCheckPoint = 1;
		SetServiceStatus(m_serviceStatusHandle, &m_serviceStatus);

		SetEvent(m_stopServiceEvent);
		return NO_ERROR;

	case SERVICE_CONTROL_PAUSE:
		break;

	case SERVICE_CONTROL_CONTINUE:
		break;

	default:
		if (controlCode >= 128 && controlCode <= 255)
			break;
		else
			break;
	}

	SetServiceStatus(m_serviceStatusHandle, &m_serviceStatus);

	return NO_ERROR;
}

// Main function to be executed as entire service code.
VOID WINAPI ServiceMain(DWORD argc, LPTSTR *argv)
{
	m_serviceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
	m_serviceStatus.dwCurrentState = SERVICE_STOPPED;
	m_serviceStatus.dwControlsAccepted = 0;
	m_serviceStatus.dwWin32ExitCode = NO_ERROR;
	m_serviceStatus.dwServiceSpecificExitCode = NO_ERROR;
	m_serviceStatus.dwCheckPoint = 0;
	m_serviceStatus.dwWaitHint = 0;

	m_serviceStatusHandle = RegisterServiceCtrlHandlerEx(SERVICE_NAME, 
		ServiceCtrlHandlerEx, NULL);
	assert(m_serviceStatusHandle);

	m_serviceStatus.dwCurrentState = SERVICE_RUNNING;
	SetServiceStatus(m_serviceStatusHandle, &m_serviceStatus);

	m_stopServiceEvent = CreateEvent(0, FALSE, FALSE, _T("SonLaServiceStopped"));
	assert(m_stopServiceEvent);
	MainTask();
	
	m_serviceStatus.dwCurrentState = SERVICE_STOP_PENDING;
	SetServiceStatus(m_serviceStatusHandle, &m_serviceStatus);

	m_serviceStatus.dwControlsAccepted &= ~(SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN);
	m_serviceStatus.dwCurrentState = SERVICE_STOPPED;
	SetServiceStatus(m_serviceStatusHandle, &m_serviceStatus);

	CloseHandle(m_stopServiceEvent);
}

VOID RunService()
{
	SERVICE_TABLE_ENTRY serviceTable[] = {
			{ SERVICE_NAME, &ServiceMain },
			{ NULL, NULL }
	};

	if (!StartServiceCtrlDispatcher(serviceTable))
		WriteLog(GetLastError());
}

VOID InstallService()
{
	SC_HANDLE serviceControlManager = OpenSCManager(0, 0, SC_MANAGER_CREATE_SERVICE);
	
	if (serviceControlManager)
	{
		TCHAR path[_MAX_PATH];		
		
		if (GetModuleFileName(NULL, path, sizeof(path) / sizeof(path[0])) > 0)
		{
			SC_HANDLE service = CreateService(
				serviceControlManager, 
				SERVICE_NAME, 
				SERVICE_DISPLAY_NAME,
				SERVICE_ALL_ACCESS, 
				SERVICE_WIN32_OWN_PROCESS,
				SERVICE_AUTO_START, 
				SERVICE_ERROR_IGNORE, 
				path,
				0, 0, 0, 0, 0);

			if (service)
			{
				printf("Successfully installed.. !\n");
				CloseServiceHandle(service);
			}
			else
			{
				printf("Create service fail\n");
				WriteLog(GetLastError());
			}
		}
		else
		{
			printf("Getmodule fail\n");
			WriteLog(GetLastError());
		}
		CloseServiceHandle(serviceControlManager);
	}
	else 
		WriteLog(GetLastError());
}

BOOL StartMyService()
{
	SC_HANDLE serviceControlManager = OpenSCManager(0, 0, SC_MANAGER_CONNECT);

	if (serviceControlManager)
	{
		SC_HANDLE service = OpenService(serviceControlManager, 
			SERVICE_NAME, SERVICE_START);		
		if (!service)
		{
			printf("Service Not Found..\n");
			return FALSE;
		}

		if (StartService(service, 0, NULL))
		{
			printf("Service started.\n");
			CloseServiceHandle(service);
			return TRUE;
		}
		else
		{
			printf("Service start fail\n");
			WriteLog(GetLastError());
		}
		CloseServiceHandle(serviceControlManager);
	}
	else
		WriteLog(GetLastError());

	return FALSE;
}

BOOL StopMyService(SC_HANDLE service)
{
	SC_HANDLE serviceControlManager = 0;

	if (!service)
	{
		serviceControlManager = OpenSCManager(0, 0, SC_MANAGER_CONNECT);
		if (!serviceControlManager)
		{
			WriteLog(GetLastError());
			return FALSE;
		}
		service = OpenService(serviceControlManager, 
			SERVICE_NAME, SERVICE_QUERY_STATUS | SERVICE_STOP);
		if (!service)
		{
			WriteLog(GetLastError());
			return FALSE;
		}
	}
	
	SERVICE_STATUS serviceStatus;
	QueryServiceStatus(service, &serviceStatus);
	if (serviceStatus.dwCurrentState != SERVICE_STOPPED)
	{
		if (ControlService(service, SERVICE_CONTROL_STOP, &serviceStatus))
		{
			printf("Stopping Service.");
			for (int i = 0; i < 100; i++)
			{
				QueryServiceStatus(service, &serviceStatus);
				if (serviceStatus.dwCurrentState == SERVICE_STOPPED)
				{
					printf("\nService stopped\n");
					return TRUE;
				}
				else
				{
					Sleep(500);
					printf(".");
				}
			}
		}
		else
			WriteLog(GetLastError());

		printf("Failed to stop service\n");
	}
	else
		printf("Cannot stop, service is not running\n");

	CloseServiceHandle(service);
	CloseServiceHandle(serviceControlManager);

	return TRUE;
}

VOID UninstallService()
{
	SC_HANDLE serviceControlManager = OpenSCManager(0, 0, SC_MANAGER_CONNECT);

	if (serviceControlManager)
	{
		SC_HANDLE service = OpenService(
			serviceControlManager, 
			SERVICE_NAME, 
			SERVICE_QUERY_STATUS | SERVICE_STOP | DELETE);

		if (service)
		{
			SERVICE_STATUS serviceStatus;
			QueryServiceStatus(service, &serviceStatus);
			if (serviceStatus.dwCurrentState != SERVICE_STOPPED)
			{
				if (!StopMyService(service))
				{
					printf("Failed to Stop Service..\n");
					return;
				}
			}
			if (DeleteService(service))
				printf("Service Successfully Removed !\n");
			else
				WriteLog(GetLastError());

			CloseServiceHandle(service);
		}
		else
			printf("Service Not Found..\n");

		CloseServiceHandle(serviceControlManager);
	}
	else
		WriteLog(GetLastError());
}

VOID MainTask()
{
	TIMEVAL	timeval = { 0, 100000 };
	fd_set fdread, fdwrite, fderror;
	
	InitService();
	if (!m_serverStatus.blListening || !m_serverStatus.blLogThreadRunning)
	{
		WriteLog(0, _T("Server cannot listening to accept request, return\n"));		
		return;
	}

#ifdef BUILD_AS_PROGRAM
	while (TRUE)
#else
	m_serviceStatus.dwControlsAccepted |= (SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN);
	SetServiceStatus(m_serviceStatusHandle, &m_serviceStatus);

	while (WaitForSingleObject(m_stopServiceEvent, 0) == WAIT_TIMEOUT)
#endif
	{
		CheckHasControlFromRemote();
#ifndef NOT_USE_DNS_MODULE
		CheckKeepDnsConnection();
#endif 
		InitFdSet(fdread, fdwrite, fderror);

		if (select(0, &fdread, &fdwrite, &fderror, &timeval) == SOCKET_ERROR)
		{
			WriteLog(WSAGetLastError());
			continue;
		}	

		for each (LPTCP_CONNECTION con in m_tcpConList)
		{
			if (con != &m_server && con != &m_dnsCon)
			{
				if (CheckDeviceNotSendDataTimeout(con))
					continue;
			}
			if (FD_ISSET(con->sock, &fdread))
			{
				if (con == &m_server) // new request connection
				{
					OnConnect();
				}
				else if (!OnRead(con)) 
				{
					OnClose(con);					
				}
			}
			if (FD_ISSET(con->sock, &fderror))
			{
				OnError(con);
			}	
#ifndef NOT_USE_DNS_MODULE
			if (FD_ISSET(con->sock, &fdwrite) && con == &m_dnsCon)
			{
				m_serverStatus.dwDnsServerStatus = DNS_STATUS_CONNECTED;
				WriteLog(0, _T("Connected with dns server\n"));
			}
#endif
		}

		for each (LPTCP_CONNECTION con in m_tcpClosedList)
		{
			if (con != &m_dnsCon)
			{
				m_tcpConList.remove(con);
				DESTROY_CONNECTION(con);
			}
			else
			{
				DESTROY_DNS_CONNECTION(con);
				m_serverStatus.blDnsSocketCreatedSuccess = FALSE;
			}
		}
		m_tcpClosedList.clear();
	}

	CleanService();
}

VOID CleanService()
{
	for each (LPTCP_CONNECTION con in m_tcpConList)
	{
		DESTROY_CONNECTION(con);
	}

#ifndef NOT_USE_DNS_MODULE
	CancelWaitableTimer(m_dnsTimer);
	CloseHandle(m_dnsTimer);
#endif
	
	m_remoteLogThread->quit();
	m_remoteLogThread->wait();
	
	m_sqlLiteThread->quit();
	m_sqlLiteThread->wait();

	delete m_remoteLogThread;
	delete m_sqlLiteThread;
	
	WSACleanup();
}

VOID InitService()
{	
	m_serverStatus.blListening = FALSE;
	m_serverStatus.blLogThreadRunning = FALSE;

	WSADATA wSAData;
	WORD version = MAKEWORD(2, 2);
	if (WSAStartup(version, &wSAData))
	{
		TCHAR msg[128];
		_stprintf_s(msg, _T("Current OS not support winsock 2.2, lastest version %d.%d\n"),
			wSAData.wHighVersion, wSAData.wVersion);
		WriteLog(0, msg);
		return;
	}

	m_server.sockAddress.sin_family = AF_INET;
	m_server.sockAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	m_server.sockAddress.sin_port = htons(SERVER_PORT);

	m_server.sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_server.sock == INVALID_SOCKET)
	{
		WriteLog(WSAGetLastError());
		return;
	}

	if (bind(m_server.sock, (SOCKADDR *)&m_server.sockAddress, sizeof(SOCKADDR_IN)) == SOCKET_ERROR)
	{
		WriteLog(WSAGetLastError());
		closesocket(m_server.sock);
		return;
	}
	if (listen(m_server.sock, BACKLOG) == SOCKET_ERROR)
	{
		WriteLog(WSAGetLastError());
		closesocket(m_server.sock);
		return;
	}
	else
	{
		m_serverStatus.blListening = TRUE;
		m_tcpConList.push_back(&m_server);
	}

	m_sqlLiteThread = new SqlLiteThread;
	m_remoteLogThread = new RemoteLogThread;

	if (m_remoteLogThread->init())
	{
		m_sqlLiteThread->start();
		m_remoteLogThread->start();
		m_serverStatus.blLogThreadRunning = TRUE;
	}

	InitDnsServer(TRUE);
	LoadDeviceInformation(g_deviceId);
}

VOID InitDnsServer(BOOL isFirstTime)
{
	if (isFirstTime)
	{
		m_serverStatus.dwDnsServerStatus = DNS_STATUS_DISCONECTED;
		m_serverStatus.blDnsSocketCreatedSuccess = FALSE;

		m_dnsCon.sockAddress.sin_family = AF_INET;
		m_dnsCon.sockAddress.sin_addr.s_addr = inet_addr(DNS_SERVER_ADDR);
		m_dnsCon.sockAddress.sin_port = htons(DNS_SERVER_PORT);

		m_tcpConList.push_back(&m_dnsCon);
	}

	if (m_dnsTimer == INVALID_HANDLE_VALUE)
	{
		m_dnsTimer = CreateWaitableTimer(NULL, FALSE, NULL);
		assert(m_dnsTimer);

		LARGE_INTEGER delayTime;
		delayTime.QuadPart = -50000000LL;
		SetWaitableTimer(m_dnsTimer, &delayTime, DNS_TIMER_INTERVAL, NULL, NULL, FALSE);
	}
	if (m_dnsWaitTimeoutTimer == INVALID_HANDLE_VALUE)
	{
		m_dnsWaitTimeoutTimer = CreateWaitableTimer(NULL, FALSE, NULL);
		assert(m_dnsWaitTimeoutTimer);
	}

	m_dnsCon.sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_dnsCon.sock == INVALID_SOCKET)
	{
		WriteLog(WSAGetLastError());
		return;
	}

	ULONG ul = 1;
	if (ioctlsocket(m_dnsCon.sock, FIONBIO, (PULONG)&ul) != SOCKET_ERROR)
	{
		m_serverStatus.blDnsSocketCreatedSuccess = TRUE;
	}
	else
	{
		WriteLog(GetLastError());
	}
}

BOOL OnConnect()
{
	LPTCP_CONNECTION con = new TCP_CONNECTION;
	SOCKADDR_IN clientAddr;
	int len = sizeof(clientAddr);

	con->sock = accept(m_server.sock, (SOCKADDR *)&clientAddr, &len);
	if (con->sock != INVALID_SOCKET)
	{
		ULONG ul = 1;
		if (ioctlsocket(con->sock, FIONBIO, (PULONG)&ul) == SOCKET_ERROR)
		{
			WriteLog(WSAGetLastError());
			delete con;
			return FALSE;
		}

		GetLocalTime(&con->lastTimeSendData);
		con->sockAddress = clientAddr;
		con->deviceId = INVALID_DEVICE_ID;
		m_tcpConList.push_back(con);

		TCHAR tmp[100];
		_stprintf_s(tmp, _T("Number connections in list %d\n"), m_tcpConList.size() - 2);
		PrintSocketInfo(clientAddr, CONNECTED, SERVER_PORT);
		WriteLog(0, tmp);
		
		return TRUE;
	}
	else
	{
		delete con;
		return FALSE;
	}	
}

BOOL OnClose(LPTCP_CONNECTION &con)
{
	m_tcpClosedList.push_back(con);

	if (con != &m_dnsCon)
	{
		PrintSocketInfo(con->sockAddress, DISCONNECTED);
	}
	else
	{
		m_serverStatus.dwDnsServerStatus = DNS_STATUS_DISCONECTED;		
		WriteLog(0, _T("Connection with dns server closed\n"));
	}
	return TRUE;
}

BOOL OnError(LPTCP_CONNECTION &con)
{
	if (std::find(m_tcpClosedList.begin(), m_tcpClosedList.end(), con) != m_tcpClosedList.end())
	{
		WriteLog(WSAGetLastError());
		OnClose(con);
	}
	return TRUE;
}

BOOL OnRead(LPTCP_CONNECTION &con)
{
	CHAR buff[READ_BUFFER_SIZE];
	ZeroMemory(buff, sizeof(buff));

	int nBytes = recv(con->sock, buff, sizeof(buff), 0);
	if (nBytes == SOCKET_ERROR)
	{
		WriteLog(WSAGetLastError());
		return WSAGetLastError() == WSAEWOULDBLOCK;
	}
	else if (nBytes > 0 && con != &m_dnsCon)
	{
#ifdef BUILD_AS_PROGRAM
		printf("New data recv:  %s\n", buff);
#endif
		GetLocalTime(&con->lastTimeSendData);
		if (buff[3] == ',')
		{
			CHAR tmp[3] = { 0 };
			memcpy_s(tmp, 3, buff, 3);
			int id = atoi(tmp);

			if (con->deviceId == INVALID_DEVICE_ID)
			{
				TCHAR tmp[128];
				_stprintf_s(tmp, _T("Device ID detected %d at %s\n"),
					id, inet_ntoa(con->sockAddress.sin_addr));
				WriteLog(0, tmp);

				con->deviceId = id;
				return nBytes;
			}			

			for (int i = 0; i < 5; i++)
			{
				if (g_deviceId[i] == con->deviceId && con->deviceId != id)
				{
					g_deviceId[i] = id;
					SaveDeviceInfomartion(g_deviceId);

					TCHAR tmp[128];
					_stprintf_s(tmp, _T("Device at %s change ID from %d to %d\n"),
						inet_ntoa(con->sockAddress.sin_addr), con->deviceId, id);
					WriteLog(0, tmp);

					con->deviceId = id;
					break;
				}
			}
		}

		m_sqlLiteThread->setNewData(strlen(buff), buff);
		m_remoteLogThread->setNewData(strlen(buff), buff);
	}

	return nBytes;
}

BOOL OnWrite(LPTCP_CONNECTION &con, PCHAR buff, DWORD buff_len)
{
	assert(buff);
	assert(buff[3] == ',');
	
	CHAR tmp[3] = { 0 };
	memcpy_s(tmp, 3, buff, 3);
	int id = atoi(tmp);
	if (id != con->deviceId)
	{
#ifdef BUILD_AS_PROGRAM
		printf("deviceId need recv is %d, but deviceId in socket %s is %d\n",
			id, inet_ntoa(con->sockAddress.sin_addr), con->deviceId);
#endif
		return 1;
	}

	buff[3] = '*';
	int nBytes = send(con->sock, buff + 3, buff_len - 3, 0);	
	if (nBytes == SOCKET_ERROR)
	{
		WriteLog(WSAGetLastError());
		return WSAGetLastError() == WSAEWOULDBLOCK;
	}
	return nBytes;
}

VOID CheckKeepDnsConnection()
{
	if (!m_serverStatus.blDnsSocketCreatedSuccess)
	{
		InitDnsServer();
		return;
	}

	if (m_serverStatus.dwDnsServerStatus == DNS_STATUS_DISCONECTED)
	{
		WriteLog(0, _T("Dns status disconnected, try reconnect ...\n"));

		int ret = connect(m_dnsCon.sock, (SOCKADDR *)&m_dnsCon.sockAddress, sizeof(SOCKADDR_IN));
		if ((ret == SOCKET_ERROR && WSAGetLastError() == WSAEWOULDBLOCK) || ret == 0)
		{
			m_serverStatus.dwDnsServerStatus = DNS_STATUS_CONNECTING;

			LARGE_INTEGER interVal;
			interVal.QuadPart = DNS_WAITING_TIMER_INTERVAL; 
			SetWaitableTimer(m_dnsWaitTimeoutTimer, &interVal, 0, NULL, NULL, FALSE);
		}
		else
		{
			WriteLog(0, _T("Try to connect with dns server error\n"));
			WriteLog(WSAGetLastError());
		}
	}
	else if (m_serverStatus.dwDnsServerStatus == DNS_STATUS_CONNECTED)
	{		
		if (WaitForSingleObject(m_dnsTimer, 0) == WAIT_OBJECT_0)
		{
#ifdef BUILD_AS_PROGRAM
			int nBytes = send(m_dnsCon.sock, DNS_DATA_SENT, strlen(DNS_DATA_SENT), 0);
			printf("Send %d bytes to dns server\n", nBytes);
#else
			send(m_dnsCon.sock, DNS_DATA_SENT, strlen(DNS_DATA_SENT), 0);
#endif
		}
	}
	else if (m_serverStatus.dwDnsServerStatus == DNS_STATUS_CONNECTING)
	{
		if (WaitForSingleObject(m_dnsWaitTimeoutTimer, 0) == WAIT_OBJECT_0)
		{
			WriteLog(0, _T("Wait for dns connection timeout, set status to disconnected\n"));
			m_serverStatus.dwDnsServerStatus = DNS_STATUS_DISCONECTED;
		}
	}
}

VOID CheckHasControlFromRemote()
{
	CHAR buff[MAX_BUFFER_SIZE];
	ZeroMemory(buff, MAX_BUFFER_SIZE);

	if (m_remoteLogThread->hasControlCmdFromClient())
	{
		DWORD nBytes = 0;
		m_remoteLogThread->getLastControlData(buff, sizeof(buff), &nBytes);
		if (nBytes > 0)
		{
			for each (LPTCP_CONNECTION con in m_tcpConList)
			{
				if (con != &m_server && con != &m_dnsCon)
					OnWrite(con, buff, nBytes);
			}
		}
	}
}

BOOL CheckDeviceNotSendDataTimeout(LPTCP_CONNECTION &con)
{
	SYSTEMTIME now;
	GetLocalTime(&now);
	if (CheckTimeOut(&con->lastTimeSendData, &now, DEVICE_NOT_SEND_DATA_TIMEOUT))
	{
		TCHAR tmp[100];
		_stprintf_s(tmp, _T("Remove connection at %s:%d because wait for data timeout\n"),
			inet_ntoa(con->sockAddress.sin_addr), con->sockAddress.sin_port);
		WriteLog(0, tmp);		
		m_tcpClosedList.push_back(con);

		return TRUE;
	}
	return FALSE;
}

VOID InitFdSet(fd_set &fdread, fd_set &fdwrite, fd_set &fderror)
{
	FD_ZERO(&fdread);
	FD_ZERO(&fdwrite);
	FD_ZERO(&fderror);
	FD_SET(m_server.sock, &fdread);

#ifndef NOT_USE_DNS_MODULE
	if (m_serverStatus.dwDnsServerStatus == DNS_STATUS_CONNECTED)
	{
		FD_SET(m_dnsCon.sock, &fdread);
		FD_SET(m_dnsCon.sock, &fderror);
	}
	else if (m_serverStatus.dwDnsServerStatus == DNS_STATUS_CONNECTING)
	{
		FD_SET(m_dnsCon.sock, &fdwrite);
	}
#endif

	for each (LPTCP_CONNECTION con in m_tcpConList)
	{
		if (con != &m_server && con != &m_dnsCon)
		{
			FD_SET(con->sock, &fdread);
			FD_SET(con->sock, &fderror);
		}
	}
}


