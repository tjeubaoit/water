#include "stdafx.h"
#include "Utils.h"
#include <stdio.h>
#include <ShlObj.h>
#include <Strsafe.h>
#include <Shlwapi.h>
#include <cstdlib>

#pragma comment(lib, "Shlwapi.lib")

LPTSTR fileName = _T("\\sonlaserver_journal");

VOID WriteTCharToFile(HANDLE hFile, size_t len, LPTSTR msgBuf, BOOL closeAfterFinish)
{
	SYSTEMTIME time;
	DWORD size = len + 100;
	TCHAR *buffToWrite = new TCHAR[size];

	GetLocalTime(&time);
	StringCchPrintf(buffToWrite, size, _T("\r\n[%d-%d-%d %d:%d:%d] "),
					time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);
	_tcscat_s(buffToWrite, size, msgBuf);
	
	DWORD dwNumberOfBytesWritten;
	WriteFile(hFile, buffToWrite, _tcslen(buffToWrite), &dwNumberOfBytesWritten, NULL);

	if (closeAfterFinish)
		CloseHandle(hFile);

	delete[] buffToWrite;
}

VOID WriteLog(DWORD enumber, LPTSTR lpMsgBuf)
{
	if (lpMsgBuf == NULL)
	{
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			enumber,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR)&lpMsgBuf,
			0,
			NULL
			);
		_tprintf(_T("Error code %d. %s"), enumber, lpMsgBuf);
	}
	else
		_tprintf(lpMsgBuf);

	TCHAR filePath[MAX_PATH];
	HANDLE hFile;

#ifdef BUILD_AS_PROGRAM
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, filePath)))
#else
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, filePath)))
#endif // BUILD_AS_PROGRAM
	{		
		_tcscat_s(filePath, _T("\\SonLaServer"));
		if (!PathFileExists(filePath))
		{
			CreateDirectory(filePath, NULL);
		}

		SYSTEMTIME time;		
		size_t size = _tcslen(fileName) + 16;
		TCHAR *buffer = new TCHAR[size];

		GetLocalTime(&time);
		StringCchPrintf(buffer, size, _T("%s_%d%d%d.log"), fileName, 
			time.wYear, time.wMonth, time.wDay);
		_tcscat_s(filePath, buffer);

		delete[] buffer;

		BYTE tryCounter = 0;
		do 
		{
			hFile = CreateFile(
				filePath,
				FILE_GENERIC_WRITE,
				FILE_SHARE_WRITE, NULL,
				OPEN_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL);

			if (hFile == INVALID_HANDLE_VALUE)
			{
				_tprintf(_T("Open log file error %d\n"), GetLastError());
				Sleep(100);
			}
			else break;
		} 
		while (++tryCounter < 5);
		
		if (hFile != INVALID_HANDLE_VALUE)
		{			
			SetFilePointer(hFile, 0, NULL, FILE_END);

			if (enumber != 0)
			{
				size_t size = _tcslen(lpMsgBuf) + 16;
				TCHAR *tmp = new TCHAR[size];
				StringCchPrintf(tmp, size, _T("Error code %d. %s"), enumber, lpMsgBuf);
				WriteTCharToFile(hFile, _tcslen(tmp), tmp, TRUE);

				delete[] tmp;
			}
			else 
				WriteTCharToFile(hFile, _tcslen(lpMsgBuf), lpMsgBuf, TRUE);
		}
	}
	else
	{
		printf("SHgetfolder error\n");
		return;
	}
}

VOID PrintSocketInfo(const SOCKADDR_IN &sockAddr, SOCKET_STATE sockState, USHORT portConnected)
{
	TCHAR buff[256];
	PCHAR tmp = inet_ntoa(sockAddr.sin_addr);
	if (sockState)
	{
		_stprintf_s(buff, _T("Socket connected: %s:%d at port: %d. "),
			tmp, sockAddr.sin_port, portConnected);
		WriteLog(0, buff);
	}
	else
	{
		_stprintf_s(buff, _T("Socket closed: %s:%d\n"), tmp, sockAddr.sin_port);
		WriteLog(0, buff);
	}
}

BOOL CheckTimeOut(const LPSYSTEMTIME startTime, const LPSYSTEMTIME endTime, DWORD timeout)
{
	if (timeout == INFINITE)
		return FALSE;

	FILETIME start, end;
	SystemTimeToFileTime(startTime, &start);
	SystemTimeToFileTime(endTime, &end);

	return (end.dwLowDateTime - start.dwLowDateTime) >= timeout;
}

VOID RemoveSpace(PCHAR src, PCHAR dst)
{
	while (*src != '\0')
	{
		*dst = *src;
		src++;
		if (*dst != ' ')
			dst++;
	}
}

BOOL LoadDeviceInformation(int *ids)
{
	LPTSTR fileName = _T("\\SonLaServer\\device_info.inf");
	TCHAR filePath[MAX_PATH];
	HANDLE hFile;

#ifdef BUILD_AS_PROGRAM
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, filePath)))
#else
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, filePath)))
#endif // BUILD_AS_PROGRAM
	{
		_tcscat_s(filePath, fileName);
		hFile = CreateFile(
			filePath, 
			FILE_GENERIC_READ, 
			FILE_SHARE_READ, 
			NULL,
			OPEN_EXISTING, 
			FILE_ATTRIBUTE_NORMAL, 
			NULL);

		if (hFile == INVALID_HANDLE_VALUE)
		{
			WriteLog(0, _T("Device infomartion config data not found\n"));
			return FALSE;
		}

		DWORD nBytes = 0;
		CHAR buffer[256] = { 0 };
		if (!ReadFile(hFile, buffer, sizeof(buffer), &nBytes, NULL))
			return FALSE;

		CHAR tmp[256] = { 0 };
		RemoveSpace(buffer, tmp);

		int splitIndex = 0, startIndex = 0;
		for (size_t i = 0; i < strlen(tmp) + 1; i++)
		{
			if ((tmp[i] == '\r' && tmp[i + 1] == '\n') || tmp[i] == 0)
			{
				CHAR deviceName[32] = { 0 };
				CHAR tmpDeviceId[8] = { 0 };
				memcpy_s(deviceName, sizeof(deviceName),
					tmp + startIndex, splitIndex - startIndex);
				memcpy_s(tmpDeviceId, sizeof(tmpDeviceId),
					tmp + (splitIndex + 1), i - splitIndex - 1);

				if (!strcmp(deviceName, "id_taybac"))
					ids[2] = atoi(tmpDeviceId);
				else if (!strcmp(deviceName, "id_km7"))
					ids[0] = atoi(tmpDeviceId);
				else if (!strcmp(deviceName, "id_banlay"))
					ids[1] = atoi(tmpDeviceId);
				else if (!strcmp(deviceName, "id_pidlog1"))
					ids[3] = atoi(tmpDeviceId);
				else if (!strcmp(deviceName, "id_pidlog2"))
					ids[4] = atoi(tmpDeviceId);
				else
				{
#ifdef  BUILD_AS_PROGRAM
					printf("Device information not found\n");
#endif //  BUILD_AS_PROGRAM
					CloseHandle(hFile);
					return FALSE;
				}

				startIndex = i + 2;
			}
			else if (tmp[i] == '=')
				splitIndex = i;
		}

		CloseHandle(hFile);
		return TRUE;
	}

	return FALSE;
}

BOOL SaveDeviceInfomartion(int *ids)
{
	LPTSTR fileName = _T("\\device_info.inf");
	TCHAR filePath[MAX_PATH];
	HANDLE hFile;

#ifdef BUILD_AS_PROGRAM
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, filePath)))
#else
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, filePath)))
#endif // BUILD_AS_PROGRAM
	{
		_tcscat_s(filePath, _T("\\SonLaServer"));
		if (!PathFileExists(filePath))
			CreateDirectory(filePath, NULL);
		_tcscat_s(filePath, fileName);

		hFile = CreateFile(
			filePath, 
			FILE_GENERIC_WRITE, 
			FILE_SHARE_WRITE, 
			NULL,
			CREATE_ALWAYS, 
			FILE_ATTRIBUTE_NORMAL, 
			NULL);

		if (hFile == INVALID_HANDLE_VALUE)
		{
			WriteLog(0, _T("Create file to save device information failed\n"));
			return FALSE;
		}

		CHAR buffToWrite[256] = { 0 };
		sprintf_s(buffToWrite,
			"id_km7=%d\r\nid_banlay = %d\r\nid_taybac=%d\r\nid_pidlog1=%d\r\nid_pidlog2=%d", 
			*(ids), *(ids + 1), *(ids + 2), *(ids + 3), *(ids + 4));

		WriteFile(hFile, buffToWrite, strlen(buffToWrite), NULL, NULL);

		CloseHandle(hFile);
		return TRUE;
	}

	return FALSE;
}

