// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#define APPLICATION_NAME				_T("SonLaServer Notifier\n")
#define SERVICE_FILE_PATH				_T("WaterServer.exe")
#define SERVICE_NAME					_T("SonLaServer")

#define SERVER_NOT_INSTALLED			0xFFFFFFFF
#define SERVER_RUNNING					SERVICE_RUNNING
#define SERVER_STOPPED					SERVICE_STOPPED
#define SERVER_STOP_PENDING				SERVICE_STOP_PENDING

#define ID_ACTION_INSTALL               100
#define ID_ACTION_UNINSTALL             200
#define ID_ACTION_START                 300
#define ID_ACTION_STOP                  400
#define ID_ADD_FIREWALL_EXCEPTION       900

#define CHECK_SERVER_STATUS_TIME_INTERVAL 1000

#define ID_TIMER                        2810
#define ID_MY_TRAY                      6969
#define WM_TRAYICON_MESSAGE             9696

//#define DEVELOPMENT_VERSION
//#define RELEASE_VERSION

// TODO: reference additional headers your program requires here
