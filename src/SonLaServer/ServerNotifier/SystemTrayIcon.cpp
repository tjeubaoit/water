#include "stdafx.h"
#include "SystemTrayIcon.h"
#include "Resource.h"
#include <Shlwapi.h>
#include <ShellAPI.h>
#include <assert.h>

#pragma comment(lib, "Shlwapi.lib")

NOTIFYICONDATA		m_niData;
HMENU				m_hMenu;
HWND				m_hWnd;
DWORD				m_status = 0;
HINSTANCE			m_hInstance;

BOOL				IsWin7OrLater();
ULONGLONG			GetDllVersion(LPCTSTR dllName);
BOOL				GetStatusText(DWORD status, LPSTR text, DWORD len);
void				UpdateTrayIconData(DWORD status);

BOOL IsWin7OrLater()
{
	// Initialize the OSVERSIONINFOEX structure.
	OSVERSIONINFOEX osvi;
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	osvi.dwMajorVersion = 6;
	osvi.dwMinorVersion = 1;

	// Initialize the condition mask.
	DWORDLONG dwlConditionMask = 0;
	VER_SET_CONDITION(dwlConditionMask, VER_MAJORVERSION, VER_GREATER_EQUAL);
	VER_SET_CONDITION(dwlConditionMask, VER_MINORVERSION, VER_GREATER_EQUAL);

	// Perform the test.
	return VerifyVersionInfo(&osvi,
		VER_MAJORVERSION | VER_MINORVERSION,
		dwlConditionMask);
}

ULONGLONG GetDllVersion(LPCTSTR dllName)
{
	assert(dllName);
	DWORDLONG ullVersion = 0;
	HINSTANCE dllInstance = LoadLibrary(dllName);
	if (dllInstance)
	{
		DLLGETVERSIONPROC pDllGetVersion;
		pDllGetVersion = (DLLGETVERSIONPROC)GetProcAddress(dllInstance, "DllGetVersion");
		if (pDllGetVersion)
		{
			DLLVERSIONINFO2 dvi;
			HRESULT result;

			ZeroMemory(&dvi, sizeof(dvi));
			dvi.info1.cbSize = sizeof(DLLVERSIONINFO2);

			result = (*pDllGetVersion)(&dvi.info1);			
			if (SUCCEEDED(result))
			{
				//ullVersion = MAKEDLLVERULL(dvi.info1.dwMajorVersion, dvi.info1.dwMinorVersion, 0, 0);
				ullVersion = dvi.ullVersion;
			}
		}
		FreeLibrary(dllInstance);
	}

	return ullVersion;
}

BOOL GetStatusText(DWORD status, LPTSTR text, DWORD len)
{
	assert(text);
	switch (status)
	{
	case SERVER_NOT_INSTALLED:
		_tcscpy_s(text, len, _T("Server service not found."));
		break;
	case SERVER_STOPPED:
		_tcscpy_s(text, len, _T("Server is stopped."));
		break;
	case SERVER_RUNNING:
		_tcscpy_s(text, len, _T("Server is running."));
		break;
	case SERVER_STOP_PENDING:
		_tcscpy_s(text, len, _T("Server is stopping."));
		break;
	default:
		return FALSE;
	}
	return TRUE;
}

void UpdateTrayIconData(DWORD status)
{
	TCHAR statusText[128];
	GetStatusText(m_status, statusText, sizeof(statusText)/sizeof(statusText[0]));
	if (_tcslen(statusText) > 0)
	{
		ZeroMemory(m_niData.szTip, sizeof(m_niData.szTip));
		_tcscpy_s(m_niData.szTip, APPLICATION_NAME);
		_tcscat_s(m_niData.szTip, statusText);

		ZeroMemory(m_niData.szInfoTitle, sizeof(m_niData.szInfoTitle));
		_tcscpy_s(m_niData.szInfoTitle, _T("Server Status Changed"));

		ZeroMemory(m_niData.szInfo, sizeof(m_niData.szInfo));
		_tcscpy_s(m_niData.szInfo, statusText);

		if (m_status == SERVER_RUNNING)
		{
			m_niData.hIcon = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDB_PNG_START));
			m_niData.dwInfoFlags = NIIF_INFO;
		}
		else
		{
			m_niData.hIcon = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDB_PNG_STOP));
			m_niData.dwInfoFlags = NIIF_WARNING;
		}

		Shell_NotifyIcon(NIM_MODIFY, &m_niData);
	}
}

SystemTrayIcon::SystemTrayIcon(HINSTANCE &hInstance, HWND &hWnd)
{
	ZeroMemory(&m_niData, sizeof(NOTIFYICONDATA));
	m_hWnd = hWnd;
	m_hInstance = hInstance;

	ULONGLONG ullVersion = GetDllVersion(_T("Shell32.dll"));
	if (ullVersion >= MAKEDLLVERULL(6, 1, 0, 0))
		m_niData.cbSize = sizeof(m_niData);
	else if (ullVersion >= MAKEDLLVERULL(6, 0, 0, 0))
		m_niData.cbSize = NOTIFYICONDATA_V3_SIZE;
	else if (ullVersion >= MAKEDLLVERULL(5, 0, 0, 0))
		m_niData.cbSize = NOTIFYICONDATA_V2_SIZE;
	else
		m_niData.cbSize = NOTIFYICONDATA_V1_SIZE;

	m_niData.uFlags = (NIF_MESSAGE | NIF_TIP | NIF_ICON | NIF_INFO);
	m_niData.uID = ID_MY_TRAY;
	m_niData.hWnd = hWnd;
	m_niData.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDB_PNG_STOP));
	m_niData.uCallbackMessage = WM_TRAYICON_MESSAGE;
	m_niData.dwState = NIS_SHAREDICON;
	m_niData.uVersion = NOTIFYICON_VERSION;

	Shell_NotifyIcon(NIM_ADD, &m_niData);

	CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
}

SystemTrayIcon::~SystemTrayIcon()
{
	Shell_NotifyIcon(NIM_DELETE, &m_niData);

	CoUninitialize();
}

void SystemTrayIcon::ShowContextMenu()
{
	m_hMenu = CreatePopupMenu();
	if (m_hMenu)
	{
		AppendMenu(m_hMenu, MF_STRING, ID_ACTION_INSTALL, _T("Install Server Service"));
		AppendMenu(m_hMenu, MF_STRING, ID_ACTION_UNINSTALL, _T("Uninstall Server"));
		AppendMenu(m_hMenu, MF_SEPARATOR, 0, NULL);
		AppendMenu(m_hMenu, MF_STRING, ID_ACTION_START, _T("Start Server"));
		AppendMenu(m_hMenu, MF_STRING, ID_ACTION_STOP, _T("Stop Server"));
		AppendMenu(m_hMenu, MF_SEPARATOR, 0, NULL);
		AppendMenu(m_hMenu, MF_STRING, ID_ADD_FIREWALL_EXCEPTION, _T("Add Exception to Firewall"));
		AppendMenu(m_hMenu, MF_SEPARATOR, 0, NULL);
		AppendMenu(m_hMenu, MF_STRING, IDM_EXIT, _T("Quit"));

		SetForegroundWindow(m_hWnd);
		POINT pt;
		GetCursorPos(&pt);
		int cmd = TrackPopupMenu(m_hMenu, TPM_BOTTOMALIGN | TPM_LEFTALIGN | TPM_LEFTBUTTON,
			pt.x, pt.y, 0, m_hWnd, NULL);

		DestroyMenu(m_hMenu);
	}
}

void SystemTrayIcon::OnContextMenuItemClick(DWORD action)
{
	LPTSTR operation = _T("open");
	if (IsWin7OrLater())
	{
		operation = _T("runas");
	}

	if (action == ID_ADD_FIREWALL_EXCEPTION)
	{
		TCHAR params1[] = _T("firewall add portopening tcp 9999 \"SonLaServer Service\" enable all");
		TCHAR params2[] = _T("firewall add portopening tcp 6969 \"SonLaServer Remote Service\" enable all");

		ShellExecute(NULL, operation, _T("netsh"), params1, NULL, SW_SHOW);
		ShellExecute(NULL, operation, _T("netsh"), params2, NULL, SW_SHOW);

		return;
	}
	else if (action == ID_ACTION_UNINSTALL && m_status == SERVER_RUNNING)
	{		
		MessageBox(m_hWnd, _T("Service should be stopped before uninstalling"),
			_T("Error"), MB_ICONERROR);
		return;
	}

	TCHAR param[32] = { 0 };
	SHELLEXECUTEINFO shExInfo = { 0 };

	shExInfo.cbSize = sizeof(shExInfo);
	shExInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	shExInfo.hwnd = 0;
	shExInfo.lpVerb = operation;
	shExInfo.lpFile = _T("sc");
	shExInfo.lpParameters = param;
	shExInfo.lpDirectory = 0;
	shExInfo.nShow = SW_SHOW;
	shExInfo.hInstApp = 0;
	
	switch (action)
	{
	case ID_ACTION_INSTALL:
		shExInfo.lpFile = SERVICE_FILE_PATH;
		_tcscpy_s(param, _T("install"));
		break;
	case ID_ACTION_UNINSTALL:
		_stprintf_s(param, _T("%s %s"), _T("delete"), SERVICE_NAME);
		break;
	case ID_ACTION_START:
		_stprintf_s(param, _T("%s %s"), _T("start"), SERVICE_NAME);
		break;
	case ID_ACTION_STOP:
		_stprintf_s(param, _T("%s %s"), _T("stop"), SERVICE_NAME);
		break;
	default:
		return;
	}

	ShellExecuteEx(&shExInfo);
}

void SystemTrayIcon::CheckServerStatus()
{
	SC_HANDLE svcCtrlManager = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);
	if (svcCtrlManager)
	{
		SC_HANDLE service = OpenService(svcCtrlManager, SERVICE_NAME, SERVICE_QUERY_STATUS);
		if (service)
		{
			SERVICE_STATUS svcStatus;
			QueryServiceStatus(service, &svcStatus);
			if (svcStatus.dwCurrentState != m_status)
			{
				m_status = svcStatus.dwCurrentState;
				UpdateTrayIconData(svcStatus.dwCurrentState);
			}
			CloseServiceHandle(service);
		}
		else if (GetLastError() == ERROR_SERVICE_DOES_NOT_EXIST && m_status != SERVER_NOT_INSTALLED)
		{
			m_status = SERVER_NOT_INSTALLED;
			UpdateTrayIconData(SERVER_NOT_INSTALLED);
		}
		CloseServiceHandle(svcCtrlManager);
	}
}