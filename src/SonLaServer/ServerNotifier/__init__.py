__author__ = 'Adm'

import abc

def overrides(interface_class):
    def overrider(method):
        assert(method.__name__ in dir(interface_class))
        return method
    return overrider


class MyParent:
    def __init__(self):
        pass

    @abc.abstractmethod
    def printAbstract(self):
        return

    def printNormal(self):
        print 'parent normal'


class MyChild(MyParent):
    def __init__(self):
        super(MyChild, self).__init__()

    @overrides(MyParent)
    def printAbstract(self):
        print 'child'

    def printNormal(self):
        print 'child normal'


if __name__ == "__main__":
    a = MyParent()
    a.printAbstract()
    a.printNormal()

    b = MyChild()
    b.printNormal()
    b.printAbstract()