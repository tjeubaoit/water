#pragma once

class SystemTrayIcon
{
public:
	SystemTrayIcon(HINSTANCE &hInstance, HWND &hWnd);
	virtual ~SystemTrayIcon();

	void ShowContextMenu();
	void OnContextMenuItemClick(DWORD action);

	static void SystemTrayIcon::CheckServerStatus();
};

