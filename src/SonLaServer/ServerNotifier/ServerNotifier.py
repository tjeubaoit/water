__author__ = 'Adm'

import sys
import win32con
from shell.shell import ShellExecuteEx
from shell import shellcon
import win32service
import pywintypes
from PySide.QtCore import *
from PySide.QtGui import *


APPLICATION_NAME = 'SonLa Server Notifier 1.0\n'

SERVICE_FILE_PATH = 'WaterServer.exe'
SERVICE_NAME = 'SonLaServer'

SERVER_NOT_INSTALLED = 0
SERVER_STOPPED = 1
SERVER_RUNNING = 4
SERVER_STOP_PENDING = 3
SERVER_MARKED_FOR_DELETION = 2

ACTION_INSTALL = 100
ACTION_UNINSTALL = 200
ACTION_START = 300
ACTION_STOP = 400

CHECK_SERVICE_STATUS_INTERVAL = 1


class ServerNotifier(QObject):
    def __init__(self, parent=None):
        super(ServerNotifier, self).__init__(parent)

        self.process = QProcess()
        self.server_status = SERVER_NOT_INSTALLED
        self.timer = QTimer()

        menu = QMenu()
        menu.addAction('Install Server Service', self, SLOT('install_server()'))
        menu.addAction('Uninstall Server', self, SLOT('uninstall_server()'))
        menu.addSeparator()
        menu.addAction('Start Server', self, SLOT('start_server()'))
        menu.addAction('Stop Server', self, SLOT('stop_server()'))
        menu.addSeparator()
        menu.addAction('Quit', self, SLOT('quit()'))

        self.tray_icon = QSystemTrayIcon()
        self.connect(self.tray_icon, SIGNAL('activated(QSystemTrayIcon.ActivationReason)'),
                     self, SLOT('icon_activated(QSystemTrayIcon.ActivationReason)'))
        self.tray_icon.setIcon(QIcon('favicon.ico'))
        self.tray_icon.setContextMenu(menu)
        self.tray_icon.setToolTip(APPLICATION_NAME + get_status_text(self.server_status))
        self.tray_icon.show()

    def __del__(self):
        if self.timer.isActive():
            self.timer.stop()
        self.tray_icon.hide()

    def start_monitoring_server(self):
        self.connect(self.timer, SIGNAL('timeout()'),
                     self, SLOT('check_server_status()'))
        self.timer.start(CHECK_SERVICE_STATUS_INTERVAL)

    def update_tray_icon(self, new_status):
        self.server_status = new_status
        text = get_status_text(self.server_status)

        icon = QSystemTrayIcon.Warning
        if self.server_status == SERVER_RUNNING:
            icon = QSystemTrayIcon.Information

        self.tray_icon.showMessage('Server Status Changed', text, icon)
        self.tray_icon.setToolTip(APPLICATION_NAME + text)

    @Slot(QSystemTrayIcon.ActivationReason)
    def icon_activated(self, reason):
        print 'tray icon activated'
        if reason != QSystemTrayIcon.Unknown:
            self.tray_icon.contextMenu().show()

    @Slot()
    def check_server_status(self):
        svc_ctrl_manager = win32service.OpenSCManager(None, None, win32service.SC_MANAGER_CONNECT)
        if svc_ctrl_manager:
            try:
                service = win32service.OpenService(svc_ctrl_manager, unicode(SERVICE_NAME),
                                                   win32service.SERVICE_QUERY_STATUS)
                status = win32service.QueryServiceStatus(service)
                if status[1] != self.server_status:
                    self.update_tray_icon(status[1])

            except pywintypes.error:
                if self.server_status != SERVER_NOT_INSTALLED:
                    print 'server uninstalled'
                    self.update_trayicon_gui(SERVER_NOT_INSTALLED)

    @Slot()
    def install_server(self):
        execute_shell(ACTION_INSTALL)

    @Slot()
    def uninstall_server(self):
        execute_shell(ACTION_UNINSTALL)

    @Slot()
    def start_server(self):
        execute_shell(ACTION_START)

    @Slot()
    def stop_server(self):
        execute_shell(ACTION_STOP)

    @Slot()
    def quit(self):
        sys.exit(0)


def get_status_text(status):
    if status == SERVER_NOT_INSTALLED:
        return 'Server service not found.'
    elif status == SERVER_STOPPED:
        return 'Server is stopped.'
    elif status == SERVER_RUNNING:
        return 'Server is running.'
    elif status == SERVER_STOP_PENDING:
        return 'Server is stopping'
    else:
        return 'Server status unknown'


def execute_shell(action):
    if action == ACTION_INSTALL:
        params = '-i'
    elif action == ACTION_UNINSTALL:
        params = '-u'
    elif action == ACTION_START:
        params = '-e'
    elif action == ACTION_STOP:
        params = '-s'
    else:
        params = None

    if params:
        ShellExecuteEx(nShow=win32con.SW_SHOWNORMAL, fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
                       lpVerb='open', lpFile=SERVICE_FILE_PATH, lpParameters=params)


# def isUserAdmin():
#
#     if os.name == 'nt':
#         import ctypes
#         # WARNING: requires Windows XP SP2 or higher!
#         try:
#             return ctypes.windll.shell32.IsUserAnAdmin()
#         except:
#             traceback.print_exc()
#             print "Admin check failed, assuming not an admin."
#             return False
#     elif os.name == 'posix':
#         # Check for root on Posix
#         return os.getuid() == 0
#     else:
#         raise RuntimeError, "Unsupported operating system for this module: %s" % (os.name,)
#
#
# def runAsAdmin(cmdLine=None, wait=True):
#
#     if os.name != 'nt':
#         raise RuntimeError, "This function is only implemented on Windows."
#
#     import win32api, win32con, win32event, win32process
#     from shell.shell import ShellExecuteEx
#     from shell import shellcon
#
#     python_exe = sys.executable
#
#     if cmdLine is None:
#         cmdLine = [python_exe] + sys.argv
#     elif type(cmdLine) not in (types.TupleType,types.ListType):
#         raise ValueError, "cmdLine is not a sequence."
#     cmd = '"%s"' % (cmdLine[0],)
#     # XXX TODO: isn't there a function or something we can call to massage command line params?
#     params = " ".join(['"%s"' % (x,) for x in cmdLine[1:]])
#     cmdDir = ''
#     showCmd = win32con.SW_SHOWNORMAL
#     #showCmd = win32con.SW_HIDE
#     lpVerb = 'runas'  # causes UAC elevation prompt.
#
#     # print "Running", cmd, params
#
#     # ShellExecute() doesn't seem to allow us to fetch the PID or handle
#     # of the process, so we can't get anything useful from it. Therefore
#     # the more complex ShellExecuteEx() must be used.
#
#     # procHandle = win32api.ShellExecute(0, lpVerb, cmd, params, cmdDir, showCmd)
#
#     procInfo = ShellExecuteEx(nShow=0,
#                               fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
#                               lpVerb=lpVerb,
#                               lpFile=cmd,
#                               lpParameters=params)
#
#     if wait:
#         procHandle = procInfo['hProcess']
#         obj = win32event.WaitForSingleObject(procHandle, win32event.INFINITE)
#         rc = win32process.GetExitCodeProcess(procHandle)
#         print "Process handle %s returned code %s" % (procHandle, rc)
#     else:
#         rc = None
#
#     return rc
#
#
# def run_application():
#     if not isUserAdmin():
#         print "You're not an admin.", os.getpid(), "params: ", sys.argv
#         #rc = runAsAdmin(["c:\\Windows\\notepad.exe"])
#         rc = runAsAdmin()
#     else:
#         print "You are an admin!", os.getpid(), "params: ", sys.argv
#         rc = 0
#     return rc


if __name__ == "__main__":
    app = QApplication(sys.argv)
    server_notifier = ServerNotifier()
    server_notifier.start_monitoring_server()
    sys.exit(app.exec_())



