#include "livegraph.h"
#include "graphicitem.h"

#include <QPainter>
#include <QDebug>
#include <QMouseEvent>
#include <QRgb>
#include <QMap>
#include <QRect>
#include <QToolTip>

#define FILL(text, len) while (text.length() < len) { text.append("0"); }

#define HEX_COLOR_1 "#117dbb"
#define HEX_COLOR_2 "#f55c18"
#define FRAME_COLOR "#d9eaf4"
#define BORDER_COLOR "#117dbb"
#define TEXT_COLOR "#787878"

#define HEADER_HEIGHT 20

QString getHexColor(int id)
{
    switch (id)
    {
    case 0: return HEX_COLOR_1;
    case 1: return HEX_COLOR_2;
    default: return QString();
    }
}

class LiveGraph::LiveGraphPrivate
{
public:
    ~LiveGraphPrivate()
    {
        delete pixmap;
    }

    QList<GraphicItem*> items;
    QMap<int, QString> headerList;
    QPixmap *pixmap;
    int itemIdCurrentHover;
    LiveGraph::GraphType type;

    int itemAt(int x, int y)
    {
        if (pixmap == 0) {
            qDebug() << "pixmap null";
            return -1;
        }

        for (int i = 0; i < items.size(); i++) {
            QColor color(items.at(i)->hexColor());
            QRgb oRgb = qRgba(color.red(), color.green(), color.blue(), color.alpha());
            QRgb rgb = pixmap->toImage().pixel(x, y);
            if (oRgb == rgb) {
                return i;
            }
        }
        return -1;
    }
};

LiveGraph::LiveGraph(QWidget *parent)
    : QWidget(parent),
      d(new LiveGraphPrivate)
{
    d->pixmap = new QPixmap(size());
    d->itemIdCurrentHover = -1;
    setMouseTracking(true);
    QToolTip::hideText();
}

LiveGraph::~LiveGraph()
{
    foreach (GraphicItem *item, d->items) {
        delete item;
    }

    delete d;
}

void LiveGraph::setType(LiveGraph::GraphType type)
{
    d->type = type;
}

void LiveGraph::setHeader(LiveGraph::HeaderLocation location, const QString &text)
{
    d->headerList.insert((int)location, text);
}

int LiveGraph::itemCounts() const
{
    return d->items.size();
}

void LiveGraph::insertItem(int itemIndex, int maxPoints, int min, int max)
{
    if (itemIndex < d->items.size()) {
        GraphicItem *item = new GraphicItem(getHexColor(itemIndex), maxPoints, min, max);
        GraphicItem *oldItem = d->items.at(itemIndex);
        d->items.removeAt(itemIndex);
        d->items.insert(itemIndex, item);
        delete oldItem;
    }
    else {
        addNewItem(maxPoints, min, max);
    }
}

void LiveGraph::addNewItem(int maxPoints, int min, int max)
{
    const int idToInsert = d->items.size();
    GraphicItem *item = new GraphicItem(getHexColor(idToInsert), maxPoints, min, max);
    d->items.append(item);
}

void LiveGraph::appendDataToItem(int index, AbstractGraphItemData *itemData)
{
    if (index < d->items.size()) {
        d->items.at(index)->addRecord(itemData);
    }
}

void LiveGraph::appendLastDataToItem(int index)
{
    if (index < d->items.size()) {
        d->items.at(index)->addLastRecord();
    }
}

void LiveGraph::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    drawFrame(&painter);
    drawGraphicItems(&painter);

    drawTextTitle(&painter);
}

void LiveGraph::drawFrame(QPainter* painter)
{
    double h = this->height() - 40;
    double w = this->width();

    painter->save();
    painter->translate(0, HEADER_HEIGHT);
    painter->fillRect(0, 0, w, h, Qt::white);

    painter->setRenderHint(QPainter::Antialiasing, false);
    painter->setPen(QColor(FRAME_COLOR));
    for (double i = 1; i < h; i += (h / 5)) {
        painter->drawLine(0, i, w, i);
    }
    for (double i = 1; i < w; i += (w / 15)) {
        painter->drawLine(i, 0, i, h);
    }

    painter->setPen(QColor(BORDER_COLOR));
    painter->drawLine(0, 0, w, 0);
    painter->drawLine(0, 0, 0, h);
    painter->drawLine(0, h, w, h);
    painter->drawLine(w - 1, 0, w - 1, h);

    painter->restore();
}

void LiveGraph::drawTextTitle(QPainter *painter)
{
    painter->save();
    painter->setPen(QColor(TEXT_COLOR));
    painter->setRenderHint(QPainter::Antialiasing);
    painter->setFont(QFont("Segoe UI", 8));

    foreach (const int location, d->headerList.keys()) {
        QString text = d->headerList.value(location);
        switch (location)
        {
        case TopLeft:
            painter->drawText(0, 15, text);
            break;

        case TopRight:
            painter->drawText(width() - 55, 15, text);
            break;

        case BottomLeft:
            painter->drawText(0, height() - 5, text);
            break;

        case BottomRight:
            painter->drawText(width() - 6, height() - 5, text);
            break;

        case Center:
            break;
        }
    }

    if (d->type == Online) {
        painter->setFont(QFont("Segoe UI", 9));
        for (int i = 0; i < itemCounts(); i++) {
            QRect rect(i * 200 + 200, 10, 5, 5);
            d->items[i]->drawInfoText(rect, painter);
        }
    }

    painter->restore();
}

void LiveGraph::drawGraphicItems(QPainter *painter)
{
    if (d->pixmap != 0) delete d->pixmap;
    d->pixmap = new QPixmap(size());
    d->pixmap->fill(Qt::transparent);

    painter->save();
    painter->translate(0, HEADER_HEIGHT);
    painter->setRenderHint(QPainter::Antialiasing);

    QPainter otherPainter(d->pixmap);
    otherPainter.translate(0, HEADER_HEIGHT);
    otherPainter.setRenderHint(QPainter::Antialiasing, false);

    foreach (GraphicItem* item, d->items) {
        QPen pen;
        pen.setColor(item->hexColor());
        if (item->isHovering()) pen.setWidth(2);
        painter->setPen(pen);
        item->draw(QRect(0, 0, width(), height() - 40), painter);

        pen.setWidth(3);
        otherPainter.setPen(pen);
        item->draw(QRect(0, 0, width(), height() - 40), &otherPainter);
    }

    painter->restore();
}

void LiveGraph::mouseMoveEvent(QMouseEvent *event)
{
    int i = d->itemAt(event->x(), event->y());    
    if (i >= 0) {
        QString text = d->items[i]->getToolTipAtPos(event->x(), rect());
        QToolTip::showText(this->mapToGlobal(event->pos()), text, parentWidget(),
                           parentWidget()->rect());

        if (i != d->itemIdCurrentHover) {
            if (d->itemIdCurrentHover >= 0) {
                d->items[d->itemIdCurrentHover]->setHovering(false);
                d->itemIdCurrentHover = -1;
            }
            d->items[i]->setHovering(true);
            d->itemIdCurrentHover = i;
            repaint();
        }
    }
    else if (i < 0 && d->itemIdCurrentHover >= 0) {
        QToolTip::hideText();
        d->items[d->itemIdCurrentHover]->setHovering(false);
        d->itemIdCurrentHover = -1;        
        repaint();        
    }
}
