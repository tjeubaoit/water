#ifndef GRAPHICITEM_H
#define GRAPHICITEM_H

#include <QList>
#include <QString>

class QPainter;
class QRect;
class AbstractGraphItemData;

class GraphicItem
{
public:
    GraphicItem(const QString&, int, int, int);
    virtual ~GraphicItem();

    void draw(const QRect &, QPainter *);
    void drawInfoText(const QRect &, QPainter *);
    void addRecord(AbstractGraphItemData* itemData);
    void addLastRecord();

    QString hexColor() const;
    void setHexColor(const QString &hexColor);

    int maxNumberData() const;
    void setMaxNumberData(int maxNumberData);

    bool isHovering() const;
    void setHovering(bool isHovering);

    void setRange(int min, int max);
    int dataCount() const;
    QString getToolTipAtPos(int dx, const QRect &container);

private:
    class GraphicItemPrivate;
    GraphicItemPrivate* const d;

    friend class LiveGraph;
    friend class GraphicItemPrivate;
};

#endif // GRAPHICITEM_H
