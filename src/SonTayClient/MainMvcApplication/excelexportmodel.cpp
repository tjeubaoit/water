#include "excelexportmodel.h"
#include "global.h"
#include "mysqlmodel.h"
#include <QtXlsx/xlsxdocument.h>


QString getColumnHeader(int columnIndex)
{
    QString result = "null";
    switch (columnIndex) {
    case TIMESERVER_ID: result = "Thời gian"; break;
    case TIMESUM_ID: result = "Thời gian chạy"; break;
    case U_ID: result = "Điện áp"; break;
    case I_ID: result = "Dòng điện"; break;
    case f_ID: result = "Tần số"; break;
    case FLOW_ID: result = "Lưu lượng"; break;
    case METTER_ID: result = "Lưu lượng tổng"; break;
    case P_ID: result = "Áp suất"; break;
    case ONOFF_ID: result = "Bật/Tắt biến tần"; break;
    }
    return result;
}


void ExcelExportModel::exportAll(const QList<Record *> &records,
                                 const QString &saveFilePath)
{
    QXlsx::Document doc;

    for (int i = 0; i < FIELD_COUNT; i++) {
        doc.write(1, i + 1, getColumnHeader(i));
    }

    int i = 0;
    foreach (Record *r, records) {
        if (r->Flow < 0 || r->Pressure < 0) continue;
        doc.write(i + 3, TIMESERVER_ID + 1, DateTime_ToString_Vie(r->TimeServer));
        doc.write(i + 3, U_ID + 1, r->Voltage);
        doc.write(i + 3, I_ID + 1, r->Amperage);
        doc.write(i + 3, FLOW_ID + 1, r->Flow);
        doc.write(i + 3, METTER_ID + 1, r->Metter);
        doc.write(i + 3, f_ID + 1, r->Frequency);
        doc.write(i + 3, TIMESUM_ID + 1, r->TimeLive);
        doc.write(i + 3, ONOFF_ID + 1, r->PowerOn ? "Bật" : "Tắt");
        doc.write(i + 3, P_ID + 1, r->Pressure);
        i++;
    }

    doc.saveAs(saveFilePath);
}
