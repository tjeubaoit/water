#ifndef MYSQLMODEL_H
#define MYSQLMODEL_H

#include <QString>
#include <QList>
#include "record.h"
#include "global.h"

#define TIMESERVER_ID 0
#define P_ID 1
#define FLOW_ID 2
#define METTER_ID 3
#define U_ID 4
#define I_ID 5
#define f_ID 6
#define TIMESUM_ID 7
#define ONOFF_ID 8

#define FLAG_INSERT_BLANK_WHEN_NULL 0x1234
#define FLAG_INSERT_LAST_VALUE_WHEN_NULL 0xabcd

class QDateTime;

class MySqlModel
{
public:
    ~MySqlModel();

    bool connect(const QString &host = DEFAULT_DB_HOST,
                 int port = DEFAULT_DB_PORT,
                 const QString &user = DEFAULT_DB_USERNAME,
                 const QString &password = DEFAULT_DB_PASSWORD);
    void disconnect();
    QString errorString();

    bool isConnected();

    bool getLastRecord(int deviceId, Record &record, int intervalSeconds = 5);
    void getMultiRecords(int deviceId, const QDateTime &first, const QDateTime &last,
                         long interValSeconds, QList<Record *> &records, int flag);
};

#endif // MYSQLMODEL_H
