#ifndef QUERYCREATOR_H
#define QUERYCREATOR_H

#include <QString>

class QDateTime;

class QueryCreator
{
public:
    static QString createQueryGetLastRecord(int deviceId, const QDateTime &startTime);

    static QString createQueryGetRecordByTime(int deviceId,
                                             const QDateTime &startTime,
                                             const QDateTime &endTime);
};

#endif // QUERYCREATOR_H
