#include "frequencycontrol.h"
#include "global.h"
#include "log.h"
#include <QTcpSocket>


FrequencyControl::FrequencyControl()
{
    m_socket = new QTcpSocket;
}

FrequencyControl::~FrequencyControl()
{
    if (m_socket) {
        m_socket->abort();
        m_socket->close();
        delete m_socket;
    }
}

void FrequencyControl::setServerInfo(const QString &host, const int &port)
{
    this->m_port = port;
    this->m_host = host;

    if (m_socket->state() == QAbstractSocket::ConnectedState) {
        Log::debug("disconnect old socket");
        m_socket->disconnectFromHost();
    }

    m_socket->connectToHost(m_host, m_port);
    Log::debug("connect to socket");
}

void FrequencyControl::sendFrequencyValueChange(int deviceId, bool isPowerOn, int val)
{
    if (m_socket->state() != QAbstractSocket::ConnectedState) {
        Log::debug("socket not connect, try connect");
        m_socket->connectToHost(m_host, m_port);
    }

    val = (val < 50) ? (val * 2) : (val * 2 - 1);

    QString deviceTxt = QString::number(deviceId);
    QString powerStateTxt = isPowerOn ? "1" : "0";
    QString valTxt = QString::number(val);
    FILL(deviceTxt, 3);
    FILL(valTxt, 2);

    QString buffer;
    buffer.append(deviceTxt + ",")
          .append(powerStateTxt + ",")
          .append(valTxt + "#");
    qDebug() << buffer;

    Log::debug("send to server " + buffer);
    int ret = m_socket->write(buffer.toUtf8());
    if (ret > 0) {
        Log::debug("send success to server " + QString::number(ret) + " bytes");
    }
}
