#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "mysqlmodel.h"
#include "appconfigmanager.h"
#include "frequencycontrol.h"

#include <QMainWindow>
#include <QHash>

namespace Ui {
class MainWindow;
}
class LiveGraph;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void connectToDatabase();

    void timer_timeout();

    void connectionDialog_valueChanged(const QStringList&);

    void on_btnExportExcel_clicked();

    void on_cbExportTimeOption_currentIndexChanged(int index);

    void on_tabWidget_currentChanged(int index);

    void on_btnExportSelectFinish_clicked();

    void on_btnFrequencyOn_clicked();

    void on_btnFrequencyOff_clicked();

    void on_spinFrequency_valueChanged(int);

private:
    void startLiveGraph();
    void loadApplicationConfig();
    void saveApplicationConfig();

    void getDateTimeRangeToExport(QDateTime &first, QDateTime &last);

    void updateInfoText(Record *record);

    Ui::MainWindow *ui;

    FrequencyControl m_frequencyControl;
    MySqlModel m_sqlModel;
    AppConfigManager m_appConfig;

    LiveGraph *m_graphOnline, *m_graphOffline;
};

#endif // MAINWINDOW_H
