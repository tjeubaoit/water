#ifndef EXCELEXPORTMODEL_H
#define EXCELEXPORTMODEL_H

#include <QList>
#include "record.h"

class ExcelExportModel
{
public:
    static void exportAll(const QList<Record *> &records,
                          const QString &saveFilePath);
};

#endif // EXCELEXPORTMODEL_H
