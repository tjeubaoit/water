#ifndef RECORD_H
#define RECORD_H

#include <QString>
#include <QDateTime>

typedef struct
{
    QDateTime TimeServer;
    qreal Voltage;
    qreal Amperage;
    qreal Frequency;
    qreal Pressure;
    qreal Flow;
    qreal Metter;
    qint64 TimeLive;
    bool PowerOn;
} Record;

inline void setDefaultRecord(Record *record, const QDateTime &datetime)
{
    record->TimeServer = datetime;
    record->TimeLive = -1;
    record->Voltage = -1;
    record->Amperage = -1;
    record->Frequency = -1;
    record->Pressure = -1;
    record->Flow = -1;
    record->Metter = -1;
}

inline Record* createBlankRecord(const QDateTime &datetime)
{
    Record *record = new Record;
    setDefaultRecord(record, datetime);
    return record;
}

#endif // RECORD_H
