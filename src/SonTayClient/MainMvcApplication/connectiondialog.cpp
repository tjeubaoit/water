#include "connectiondialog.h"
#include "ui_connectiondialog.h"
#include <QStringList>

ConnectionDialog::ConnectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConnectionDialog)
{
    ui->setupUi(this);
}

ConnectionDialog::~ConnectionDialog()
{
    delete ui;
}

void ConnectionDialog::on_btnOk_clicked()
{
    QStringList list;
    list.append(ui->editServerAddr->text());
    list.append(ui->editUsername->text());
    list.append(ui->editPassword->text());

    emit _connectionDialog_valueChanged(list);

    QDialog::done(QDialog::Accepted);
}

void ConnectionDialog::on_btnCancel_clicked()
{
    QDialog::done(QDialog::Rejected);
}
