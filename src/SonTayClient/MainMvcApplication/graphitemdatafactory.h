#ifndef GRAPHITEMDATAFACTORY_H
#define GRAPHITEMDATAFACTORY_H

#include "pressuregraphitemdata.h"
#include "flowgraphicitemdata.h"
#include "global.h"
#include <QVariant>

inline AbstractGraphItemData* createGraphItemData(int type, const QVariant &data)
{
    switch (type)
    {
    case TYPE_GRAPH_PRESSURE:
        return new PressureGraphItemData(data[0].toDouble())
    case TYPE_GRAPH_FLOW:
        return new FlowGraphicItemData;
    default:
        return 0;
    }
}

#endif // GRAPHITEMDATAFACTORY_H
