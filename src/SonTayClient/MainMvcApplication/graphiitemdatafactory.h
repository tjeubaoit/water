#ifndef GRAPHIITEMDATAFACTORY_H
#define GRAPHIITEMDATAFACTORY_H

#include "record.h"
#include "global.h"
#include "pressuregraphitemdata.h"
#include "flowgraphicitemdata.h"

inline AbstractGraphItemData* createGraphItemData(int type, const Record &record)
{
    switch (type)
    {
    case TYPE_GRAPH_PRESSURE:
        return new PressureGraphItemData(record.Pressure, record.TimeServer);
    case TYPE_GRAPH_FLOW:
        return new FlowGraphicItemData(record.Flow, record.TimeServer);
    default:
        return 0;
    }
}

#endif // GRAPHIITEMDATAFACTORY_H
