HEADERS += \
    mysqlmodel.h \
    record.h \
    querycreator.h \
    excelexportmodel.h \
    frequencycontrol.h \
    pressuregraphitemdata.h \
    flowgraphicitemdata.h \
    graphiitemdatafactory.h

SOURCES += \
    mysqlmodel.cpp \
    querycreator.cpp \
    excelexportmodel.cpp \
    frequencycontrol.cpp \
    pressuregraphitemdata.cpp \
    flowgraphicitemdata.cpp
