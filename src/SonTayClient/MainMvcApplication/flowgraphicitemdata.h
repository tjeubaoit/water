#ifndef FLOWGRAPHICITEMDATA_H
#define FLOWGRAPHICITEMDATA_H

#include "abstractgraphitemdata.h"
#include <QDateTime>

class FlowGraphicItemData : public AbstractGraphItemData
{
public:
    FlowGraphicItemData(qreal flow, QDateTime datetime);

    virtual QVariant getValue() const;
    virtual QStringList getToolTipText() const;
    virtual bool larger(const AbstractGraphItemData*) const;

private:
    qreal m_flow;
    QDateTime m_datetime;
};

#endif // FLOWGRAPHICITEMDATA_H
