#include "querycreator.h"
#include "global.h"
#include <QDateTime>

#define get_table_name(deviceId) ".data" + QString::number(deviceId)

QString QueryCreator::createQueryGetLastRecord(int deviceId, const QDateTime &startTime)
{
    QString startTimeText = DateTime_ToString_Us(startTime);
    QString result;
    result.append("SELECT TIMESERVER,P,Flow,Metter,U,I,f,TimeSum,ONOFF FROM ")
          .append(DATABASE_NAME)
          .append(get_table_name(deviceId))
          .append(" WHERE TIMESERVER >= ")
          .append("'" + startTimeText + "'")
          .append(" ORDER BY TIMESERVER desc LIMIT 1");

    return result;
}

QString QueryCreator::createQueryGetRecordByTime(int deviceId,
                                                const QDateTime &startTime,
                                                const QDateTime &endTime)
{
    QString result;
    QString startTimeText = DateTime_ToString_Us(startTime);
    QString endTimeText = DateTime_ToString_Us(endTime);

    result.append("SELECT TIMESERVER,P,Flow,Metter,U,I,f,TimeSum,ONOFF FROM ")
            .append(DATABASE_NAME)
            .append(get_table_name(deviceId))
            .append(" WHERE TIMESERVER >= ")
            .append("'" + startTimeText + "'")
            .append(" AND TIMESERVER <= ")
            .append("'" + endTimeText + "'");

    return result;
}
