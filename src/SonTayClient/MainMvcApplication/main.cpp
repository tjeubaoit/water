#include <QApplication>
#include <QDir>
#include <QStandardPaths>
#include <QIcon>
#include "mainwindow.h"
#include "global.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setWindowIcon(QIcon(url_application_icon));

    QString path = QStandardPaths::standardLocations(QStandardPaths::DataLocation).at(0);
    QDir dir(path);
    if (!dir.exists()) {
        dir.mkpath(path);
    }

    MainWindow w;
    w.show();

    return app.exec();
}
