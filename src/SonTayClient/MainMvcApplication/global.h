#ifndef GLOBAL_H
#define GLOBAL_H

#define APPLICATION_NAME "Quản lý trạm nước"
#define APPLICATION_VERSION "1.0"
#define APPLICATION_ORGANIZATION "V-tek"
#define url_application_icon ":/favicon.ico"

#define DEFAULT_DB_HOST "localhost"
#define DEFAULT_DB_PORT 3306
#define DATABASE_NAME "sontaylog"
#define DEFAULT_DB_USERNAME "root"
#define DEFAULT_DB_PASSWORD "1234"

#define DEFAULT_SOCKET_HOST "127.0.0.1"
#define DEFAULT_SOCKET_PORT 9999

#define FIELD_COUNT 9

#define LIVE_GRAPH_INTEVERVAL 5000 // mili seconds
#define LIVE_GRAPH_COUNT 720
#define EXPORT_EXCEL_INTERVAL 300 // seconds

#define PRESSURE_MIN 0
#define PRESSURE_MAX 15

#define FLOW_MIN 0
#define FLOW_MAX 150

#define TYPE_GRAPH_PRESSURE 0
#define TYPE_GRAPH_FLOW 1

#define URL_CONFIG_FILE QStandardPaths::standardLocations(QStandardPaths::DataLocation).at(0) + "/sontay.conf"

#define DateTime_ToString_Us(datetime) datetime.toString("yyyy-MM-dd hh:mm:ss")
#define DateTime_ToString_Vie(datetime) datetime.toString("dd-MM-yyyy hh:mm")
#define FILL(text, len) while (text.length() < len) { text.insert(0, "0"); }

#endif // GLOBAL_H
