#include "mysqlmodel.h"
#include "querycreator.h"
#include "log.h"
#include "global.h"
#include <QSqlDatabase>
#include <QSqlError>
#include <QDebug>
#include <QSqlQuery>
#include <QDateTime>


static QSqlDatabase getDefaultDatabase() {
    return QSqlDatabase::database(DATABASE_NAME);
}

static void floorDateTime(QDateTime &datetime)
{
    int minute = datetime.toString("mm").toInt();
    int hour = datetime.toString("hh").toInt();
    while (minute % 5 != 0) {
        minute ++;
    }
    QTime time(hour, minute);
    datetime.setTime(time);
}

MySqlModel::~MySqlModel()
{
    QSqlDatabase::removeDatabase(DATABASE_NAME);
}

bool MySqlModel::connect(const QString &host, int port,
                         const QString &user, const QString &password)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL", DATABASE_NAME);
    db.setHostName(host);
    db.setPort(port);
    db.setDatabaseName(DATABASE_NAME);

    return db.open(user, password);
}

void MySqlModel::disconnect()
{
    QSqlDatabase db = getDefaultDatabase();
    if (db.isOpen()) {
        db.close();
    }
}

bool MySqlModel::getLastRecord(int deviceId, Record &record, int intervalSeconds)
{
    QString cmd = QueryCreator::createQueryGetLastRecord(deviceId,
            QDateTime::currentDateTime().addSecs(intervalSeconds * -1));
    QSqlQuery query(getDefaultDatabase());
    if (!query.exec(cmd)) {
        Log::error("query last record fail, " + query.lastError().text());
        return false;
    }

    if (query.next() && query.isValid()) {
        record.TimeServer = query.value(TIMESERVER_ID).toDateTime();
        record.Voltage = query.value(U_ID).toDouble();
        record.Amperage = query.value(I_ID).toDouble();
        record.Frequency = query.value(f_ID).toDouble();
        record.Flow = query.value(FLOW_ID).toDouble();
        record.Metter = query.value(METTER_ID).toDouble();
        record.Pressure = query.value(P_ID).toDouble();
        record.TimeLive = query.value(TIMESUM_ID).toLongLong();
        record.PowerOn = query.value(ONOFF_ID).toBool();
        return true;
    }
    return false;
}

void MySqlModel::getMultiRecords(int deviceId, const QDateTime &first,
                                 const QDateTime &last, long interValSeconds,
                                 QList<Record *> &records, int flag)
{
    QSqlQuery query(getDefaultDatabase());
    QString cmd = QueryCreator::createQueryGetRecordByTime(
                deviceId, first, last);
    if (!query.exec(cmd)) {
        Log::error("query multi record fail, " + query.lastError().text());
        return;
    }

    QDateTime next = first;
    if (interValSeconds > 60)
        floorDateTime(next);
    Record *tmp = 0;

    while (next < last) {
        if (query.next() && query.isValid()) {
            QDateTime timeServer = query.value(TIMESERVER_ID).toDateTime();
            while (timeServer > next.addSecs(interValSeconds)) {
                if (flag == FLAG_INSERT_BLANK_WHEN_NULL || tmp == 0) {
                    Record *record = createBlankRecord(next);
                    records.append(record);
                }
                else {
                    records.append(tmp);
                }
                next = next.addSecs(interValSeconds);
            }
            if (timeServer < next) {
                continue;
            }

            Record *record = new Record;
            record->TimeServer = timeServer;
            record->Voltage = query.value(U_ID).toDouble();
            record->Amperage = query.value(I_ID).toDouble();
            record->Frequency = query.value(f_ID).toDouble();
            record->Flow = query.value(FLOW_ID).toDouble();
            record->Metter = query.value(METTER_ID).toDouble();
            record->Pressure = query.value(P_ID).toDouble();
            record->TimeLive = query.value(TIMESUM_ID).toLongLong();
            record->PowerOn = query.value(ONOFF_ID).toBool();
            tmp = record;
            records.append(record);
            next = next.addSecs(interValSeconds);
        }
        else {
            if (flag == FLAG_INSERT_BLANK_WHEN_NULL || tmp == 0) {
                Record *record = createBlankRecord(next);
                records.append(record);
            }
            else {
                records.append(tmp);
            }
            next = next.addSecs(interValSeconds);
        }
    }
}

QString MySqlModel::errorString()
{
    QSqlDatabase db = getDefaultDatabase();
    return db.lastError().text();
}

bool MySqlModel::isConnected()
{
    QSqlDatabase db = getDefaultDatabase();
    return db.isOpen();
}
