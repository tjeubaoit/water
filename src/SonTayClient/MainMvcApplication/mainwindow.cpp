#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "livegraph.h"
#include "record.h"
#include "global.h"
#include "excelexportmodel.h"
#include "connectiondialog.h"
#include "graphiitemdatafactory.h"
#include "log.h"

#include <QTimer>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QStandardPaths>
#include <QFile>
#include <QButtonGroup>

#define EXPORT_TIME_LAST_DAY 0
#define EXPORT_TIME_LAST_WEEK 1
#define EXPORT_TIME_LAST_MONTH 2
#define EXPORT_TIME_CUSTOM 3

#define GET_LAST_RECORD_MAX_FAIL 720

static QTimer timer;
static QString getDateTimeRangeString(const QDateTime &first, const QDateTime &last)
{
    int day = first.daysTo(last);
    if (day > 0) {
        return QString::number(day) + " ngày";       
    }

    qint64 seconds = first.secsTo(last);
    return QString::number(seconds / 3600 + 1) + " giờ";
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_appConfig(URL_CONFIG_FILE)
{
    ui->setupUi(this);
    ui->dateTimeLast->setDateTime(QDateTime::currentDateTime());

    QButtonGroup *group = new QButtonGroup(this);
    group->addButton(ui->btnFrequencyOff);
    group->addButton(ui->btnFrequencyOn);
    group->setExclusive(true);

    m_graphOnline = new LiveGraph(this);
    m_graphOnline->setHeader(LiveGraph::BottomLeft, "60 phút");
    m_graphOnline->setHeader(LiveGraph::BottomRight, "0");
    m_graphOnline->setHeader(LiveGraph::TopLeft, "150 m3/h");
    m_graphOnline->setHeader(LiveGraph::TopRight, "15 kg/cm2");
    m_graphOnline->setType(LiveGraph::Online);

    m_graphOffline = new LiveGraph(this);
    m_graphOffline->setHeader(LiveGraph::BottomRight, "0");
    m_graphOffline->setHeader(LiveGraph::TopLeft, "150 m3/h");
    m_graphOffline->setHeader(LiveGraph::TopRight, "15 kg/cm2");
    m_graphOffline->setType(LiveGraph::Offline);

    QHBoxLayout *hLayoutTabLive = new QHBoxLayout;
    hLayoutTabLive->addWidget(m_graphOnline);
    ui->frameGraph->setLayout(hLayoutTabLive);

    QHBoxLayout *hLayoutTabStatistic = new QHBoxLayout;
    hLayoutTabStatistic->addWidget(m_graphOffline);
    ui->frameGraphStatistic->setLayout(hLayoutTabStatistic);

    QTimer::singleShot(1000, this, SLOT(connectToDatabase()));

    this->loadApplicationConfig();
}

MainWindow::~MainWindow()
{
    this->saveApplicationConfig();

    m_sqlModel.disconnect();

    delete ui;
}

void MainWindow::loadApplicationConfig()
{
    if (m_appConfig.hasKey("onoff")) {
        bool deviceFrequencyState = m_appConfig.get("onoff").toInt() == 0
                ? false : true;
        ui->btnFrequencyOn->setChecked(deviceFrequencyState);
        ui->btnFrequencyOff->setChecked(!deviceFrequencyState);
    }
    if (m_appConfig.hasKey("frequency")) {
        int deviceFrequency = m_appConfig.get("frequency").toInt();
        ui->spinFrequency->setValue(deviceFrequency);
    }

    if (m_appConfig.hasKey("socket_host")) {
        QString host = m_appConfig.get("socket_host");
        int port = m_appConfig.get("socket_port").toInt();
        m_frequencyControl.setServerInfo(host, port);
    }
    else {
        m_frequencyControl.setServerInfo(DEFAULT_SOCKET_HOST, DEFAULT_SOCKET_PORT);
        m_appConfig.put("socket_host", DEFAULT_SOCKET_HOST);
        m_appConfig.put("socket_port", QString::number(DEFAULT_SOCKET_PORT));
    }
}

void MainWindow::saveApplicationConfig()
{
    int val = ui->btnFrequencyOn->isChecked() ? 1 : 0;
    m_appConfig.put("onoff", QString::number(val));
    m_appConfig.put("frequency", QString::number(ui->spinFrequency->value()));
    m_appConfig.saveConfigToFile();
}

void MainWindow::getDateTimeRangeToExport(QDateTime &first, QDateTime &last)
{
    last = QDateTime::currentDateTime();

    switch (ui->cbExportTimeOption->currentIndex())
    {
    case EXPORT_TIME_LAST_DAY:
        first = last.addDays(-1);
        break;

    case EXPORT_TIME_LAST_WEEK:
        first = last.addDays(-7);
        break;

    case EXPORT_TIME_LAST_MONTH:
        first = last.addMonths(-1);
        break;

    default:
        first = ui->dateTimeFirst->dateTime();
        last = ui->dateTimeLast->dateTime();
        break;
    }
}

void MainWindow::updateInfoText(Record *record)
{
    if (record->Pressure >= 0) {
        ui->lbAmperage->setText(QString::number(record->Amperage) + " A");
        ui->lbVoltage->setText(QString::number(record->Voltage) + " V");
        ui->lbMetter->setText(QString::number(record->Metter) + " m3");
        ui->lbFrequency->setText(QString::number(record->Frequency) + " Hz");
        ui->lbTimeSum->setText(QString::number(record->TimeLive) + " h");
    }
    else {
        ui->lbAmperage->setText("---");
        ui->lbVoltage->setText("---");
        ui->lbMetter->setText("---");
        ui->lbFrequency->setText("---");
        ui->lbTimeSum->setText("---");
    }
}

void MainWindow::timer_timeout()
{    
    static int timeoutCounter = -1;
    Record record;
    setDefaultRecord(&record, QDateTime::currentDateTime());
    if (m_sqlModel.getLastRecord(301, record)) {
        timeoutCounter = -1;
        updateInfoText(&record);
        Log::debug("new live record " + QString::number(record.Pressure)
                   + " " + QString::number(record.Flow));
    }
    else {
        timeoutCounter = (timeoutCounter == INT_MAX)
                ? GET_LAST_RECORD_MAX_FAIL + 1 : timeoutCounter + 1;
        Log::debug("no new live record");
    }

    if (timeoutCounter >= 0 && timeoutCounter < GET_LAST_RECORD_MAX_FAIL) {
        m_graphOnline->appendLastDataToItem(TYPE_GRAPH_PRESSURE);
        m_graphOnline->appendLastDataToItem(TYPE_GRAPH_FLOW);
    }
    else {
        if (timeoutCounter > GET_LAST_RECORD_MAX_FAIL) {
            ui->lbAmperage->setText("---");
            ui->lbVoltage->setText("---");
            ui->lbMetter->setText("---");
            ui->lbFrequency->setText("---");
            ui->lbTimeSum->setText("---");
        }
        m_graphOnline->appendDataToItem(TYPE_GRAPH_PRESSURE,
                createGraphItemData(TYPE_GRAPH_PRESSURE, record));
        m_graphOnline->appendDataToItem(TYPE_GRAPH_FLOW,
                createGraphItemData(TYPE_GRAPH_FLOW, record));
    }
    m_graphOnline->update();
}

void MainWindow::connectionDialog_valueChanged(const QStringList &list)
{
    Log::debug("connection parameters change: host=" + list.at(0)
               + " user=" + list.at(1) + " pass=" + list.at(2));
    m_appConfig.put("db_host", list.at(0));
    m_appConfig.put("db_user", list.at(1));
    m_appConfig.put("db_password", list.at(2));
}

void MainWindow::on_btnExportExcel_clicked()
{
    QString dir = QFileDialog::getSaveFileName(
                this, "Save", "", "Excel Workbook (*.xlsx)");
    if (dir.isNull() || dir.isEmpty()) {
        return;
    }

    QDateTime firstTime, lastTime;
    getDateTimeRangeToExport(firstTime, lastTime);

    QList<Record *> records;
    m_sqlModel.getMultiRecords(301, firstTime, lastTime, EXPORT_EXCEL_INTERVAL, records,
                               FLAG_INSERT_BLANK_WHEN_NULL);
    Log::debug("export excel from " + DateTime_ToString_Us(firstTime)
               + " to " + DateTime_ToString_Us(lastTime)
               + " with records size=" + QString::number(records.size()));
    ExcelExportModel::exportAll(records, dir);
}

void MainWindow::connectToDatabase()
{
    if (m_appConfig.hasKey("db_host")) {
        QString host = m_appConfig.get("db_host");
        int port = m_appConfig.get("db_port").toInt();
        QString user = m_appConfig.get("db_user");
        QString pass = m_appConfig.get("db_password");
        m_sqlModel.connect(host, port, user, pass);
    }
    else m_sqlModel.connect();

    if (!m_sqlModel.isConnected()) {
        Log::error("cannot connect to database, " + m_sqlModel.errorString());
        QMessageBox::critical(this, "Lỗi", "Không thể kết nối tới máy chủ dữ liệu",
                              QMessageBox::Ok);
        ConnectionDialog connectionDialog(this);
        connect(&connectionDialog, SIGNAL(_connectionDialog_valueChanged(QStringList)),
                this, SLOT(connectionDialog_valueChanged(QStringList)));

        int ret = connectionDialog.exec();
        if (ret == QDialog::Accepted) {
            this->connectToDatabase();
        }
        else {
            Log::error("cannot connect db, user refure try connect, exit app");
            this->hide();
            qApp->quit();
        }
    }
    else startLiveGraph();
}

void MainWindow::startLiveGraph()
{
    QList<Record *> records;
    QDateTime first = QDateTime::currentDateTime().addSecs(-3600);
    m_sqlModel.getMultiRecords(301, first, QDateTime::currentDateTime(), 5, records,
                               FLAG_INSERT_LAST_VALUE_WHEN_NULL);
    Log::debug("get init values for live graph");

    m_graphOnline->addNewItem(LIVE_GRAPH_COUNT, PRESSURE_MIN, PRESSURE_MAX);
    m_graphOnline->addNewItem(LIVE_GRAPH_COUNT, FLOW_MIN, FLOW_MAX);

    if (!records.isEmpty()) {
        Record *record = records.last();
        updateInfoText(record);

        foreach (Record *r, records) {
            m_graphOnline->appendDataToItem(TYPE_GRAPH_PRESSURE,
                        createGraphItemData(TYPE_GRAPH_PRESSURE, *r));
            m_graphOnline->appendDataToItem(TYPE_GRAPH_FLOW,
                        createGraphItemData(TYPE_GRAPH_FLOW, *r));
        }
        m_graphOnline->update();
    }

    connect(&timer, SIGNAL(timeout()),
            this, SLOT(timer_timeout()));
    timer.start(LIVE_GRAPH_INTEVERVAL);
}

void MainWindow::on_cbExportTimeOption_currentIndexChanged(int index)
{
    if (index == EXPORT_TIME_CUSTOM) {
        ui->dateTimeFirst->show();
        ui->dateTimeLast->show();
        ui->lbDateTimeFirst->show();
        ui->lbDateTimeLast->show();
        ui->btnExportSelectFinish->show();
    }
    else {
        ui->dateTimeFirst->hide();
        ui->dateTimeLast->hide();
        ui->lbDateTimeFirst->hide();
        ui->lbDateTimeLast->hide();
        ui->btnExportSelectFinish->hide();
    }

    if (m_sqlModel.isConnected()) {
        QDateTime firstTime, lastTime;
        getDateTimeRangeToExport(firstTime, lastTime);
        QList<Record *> records;
        m_sqlModel.getMultiRecords(301, firstTime, lastTime, EXPORT_EXCEL_INTERVAL,
                                   records, FLAG_INSERT_BLANK_WHEN_NULL);
        Log::debug("get multi records from " + DateTime_ToString_Us(firstTime)
                   + " to " + DateTime_ToString_Us(lastTime));

        m_graphOffline->setHeader(LiveGraph::BottomLeft,
                                  getDateTimeRangeString(firstTime, lastTime));
        m_graphOffline->insertItem(TYPE_GRAPH_PRESSURE, records.size() - 1,
                                   PRESSURE_MIN, PRESSURE_MAX);
        m_graphOffline->insertItem(TYPE_GRAPH_FLOW, records.size() - 1,
                                   FLOW_MIN, FLOW_MAX);

        if (!records.isEmpty()) {
            foreach (Record *r, records) {
                m_graphOffline->appendDataToItem(TYPE_GRAPH_PRESSURE,
                            createGraphItemData(TYPE_GRAPH_PRESSURE, *r));
                m_graphOffline->appendDataToItem(TYPE_GRAPH_FLOW,
                            createGraphItemData(TYPE_GRAPH_FLOW, *r));
            }
            m_graphOffline->update();
        }
    }
}

void MainWindow::on_tabWidget_currentChanged(int)
{
    if (m_graphOffline->itemCounts() == 0) { // only query at first time
        Log::debug("init tab statistic at first");
        on_cbExportTimeOption_currentIndexChanged(ui->cbExportTimeOption->currentIndex());
    }
}

void MainWindow::on_btnExportSelectFinish_clicked()
{
    Log::debug("btn select finish clicked");
    on_cbExportTimeOption_currentIndexChanged(ui->cbExportTimeOption->currentIndex());
}

void MainWindow::on_btnFrequencyOn_clicked()
{
    Log::debug("user switch on control frequency");
    on_spinFrequency_valueChanged(0);    
}

void MainWindow::on_btnFrequencyOff_clicked()
{
    Log::debug("user switch off control frequency");
    on_spinFrequency_valueChanged(0);
}

void MainWindow::on_spinFrequency_valueChanged(int)
{
    Log::debug("user control frequency to device, value="
               + QString::number(ui->spinFrequency->value()));
    m_frequencyControl.sendFrequencyValueChange(301, ui->btnFrequencyOn->isChecked(),
                                     ui->spinFrequency->value());
}
