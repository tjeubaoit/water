#ifndef PRESSUREGRAPHITEMDATA_H
#define PRESSUREGRAPHITEMDATA_H

#include "abstractgraphitemdata.h"
#include <QDateTime>

class PressureGraphItemData : public AbstractGraphItemData
{
public:
    PressureGraphItemData(qreal pressure, QDateTime datetime);

    virtual QVariant getValue() const;
    virtual QStringList getToolTipText() const;
    virtual bool larger(const AbstractGraphItemData*) const;

private:
    qreal m_pressure;
    QDateTime m_datetime;
};

#endif // PRESSUREGRAPHITEMDATA_H
