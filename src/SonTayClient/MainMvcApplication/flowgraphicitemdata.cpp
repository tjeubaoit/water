#include "flowgraphicitemdata.h"
#include "global.h"

FlowGraphicItemData::FlowGraphicItemData(qreal flow, QDateTime datetime)
{
    this->m_flow = flow;
    this->m_datetime = datetime;
}

QVariant FlowGraphicItemData::getValue() const
{
    return m_flow;
}

QStringList FlowGraphicItemData::getToolTipText() const
{
    QStringList list;
    list.append("Thời gian: " + DateTime_ToString_Vie(m_datetime));
    if (m_flow >= 0) {
        list.append("Lưu lượng: " + QString::number(m_flow) + " m3/h");
    } else {
        list.append("Lưu lượng:");
    }
    return list;
}

bool FlowGraphicItemData::larger(const AbstractGraphItemData *other) const
{
    return m_datetime > other->getValue().toDateTime();
}
