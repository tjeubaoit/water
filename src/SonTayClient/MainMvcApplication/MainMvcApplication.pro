#-------------------------------------------------
#
# Project created by QtCreator 2014-09-03T10:09:08
#
#-------------------------------------------------

QT       += core gui sql xlsx network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sontayclient
TEMPLATE = app


SOURCES += main.cpp \
    mainwindow.cpp \
    connectiondialog.cpp

HEADERS  += global.h \
    mainwindow.h \
    connectiondialog.h

include(model.pri)

RESOURCES += \
    images.qrc

FORMS += \
    connectiondialog.ui \
    mainwindow.ui

RC_ICONS = water.ico

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../LiveGraph/release/ -lvtlivegraph
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../LiveGraph/debug/ -lvtlivegraph
else:unix: LIBS += -L$$OUT_PWD/../LiveGraph/ -lvtlivegraph

INCLUDEPATH += $$PWD/../LiveGraph
DEPENDPATH += $$PWD/../LiveGraph


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../lib/IOHelper/release/ -lvtio
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../lib/IOHelper/debug/ -lvtio

INCLUDEPATH += $$PWD/../../../lib/IOHelper/include
DEPENDPATH += $$PWD/../../../lib/IOHelper/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Logging/release/ -lvtlogging
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Logging/debug/ -lvtlogging
else:unix: LIBS += -L$$OUT_PWD/../Logging/ -lvtlogging

INCLUDEPATH += $$PWD/../Logging
DEPENDPATH += $$PWD/../Logging
