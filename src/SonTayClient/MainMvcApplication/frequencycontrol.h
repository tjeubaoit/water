#ifndef FREQUENCYCONTROL_H
#define FREQUENCYCONTROL_H

#include <QObject>
#include <QString>
#include "global.h"

class QTcpSocket;

class FrequencyControl
{
public:
    explicit FrequencyControl();
    ~FrequencyControl();

    void setServerInfo(const QString &host, const int &port);
    void sendFrequencyValueChange(int deviceId, bool isPowerOn, int val);

private:
    QString m_host;
    int m_port;
    QTcpSocket *m_socket;

};

#endif // FREQUENCYCONTROL_H
