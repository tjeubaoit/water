#include "pressuregraphitemdata.h"
#include "global.h"

PressureGraphItemData::PressureGraphItemData(qreal pressure, QDateTime datetime)
{
    this->m_pressure = pressure;
    this->m_datetime = datetime;
}

QVariant PressureGraphItemData::getValue() const
{
    return m_pressure;
}

QStringList PressureGraphItemData::getToolTipText() const
{
    QStringList list;
    list.append("Thời gian: " + DateTime_ToString_Vie(m_datetime));
    if (m_pressure >= 0) {
        list.append("Áp suất: " + QString::number(m_pressure) + " kg/cm2");
    } else {
        list.append("Áp suất:");
    }
    return list;
}

bool PressureGraphItemData::larger(const AbstractGraphItemData *other) const
{
    return m_datetime > other->getValue().toDateTime();
}
