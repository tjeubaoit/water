#ifndef LOG_H
#define LOG_H

#include "logging_global.h"

class LOGGINGSHARED_EXPORT Log
{

public:
    static void debug(const QString &obj);
    static void error(const QString &obj);

};

#endif // LOG_H
