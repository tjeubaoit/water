#include "log.h"
#include <QFile>
#include <QStandardPaths>
#include <QVariant>
#include <QDateTime>


void Log::debug(const QString &obj)
{
    QFile file(QStandardPaths::standardLocations(QStandardPaths::DataLocation).at(0)
               + "/sontay.log");
    if (file.open(QIODevice::Append | QIODevice::Text)) {
        QString text = QDateTime::currentDateTime().toString("[yyyy-MM-dd hh:mm:ss]");
        text.append("  ").append(obj).append("\n");
        file.write(text.toUtf8());
        file.close();
    }
}

void Log::error(const QString &obj)
{
    QFile file(QStandardPaths::standardLocations(QStandardPaths::DataLocation).at(0)
               + "/sontay.err");
    if (file.open(QIODevice::Append | QIODevice::Text)) {
        QString text = QDateTime::currentDateTime().toString("[yyyy-MM-dd hh:mm:ss]");
        text.append("  ").append(obj).append("\n");
        file.write(text.toUtf8());
        file.close();
    }
}
