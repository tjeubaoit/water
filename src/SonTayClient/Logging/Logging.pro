#-------------------------------------------------
#
# Project created by QtCreator 2014-09-13T15:44:53
#
#-------------------------------------------------

QT       -= gui

TARGET = vtlogging
TEMPLATE = lib

DEFINES += LOGGING_LIBRARY

SOURCES += log.cpp

HEADERS += log.h\
        logging_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
